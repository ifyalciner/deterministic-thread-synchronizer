# Ferguson

Best butler for robot behavioral software development

# Class Diagram

```plantuml
@startuml

package synchronizers <<frame>> #DDDDDD {
    namespace Synchronizer {
      interface Synchronizer{
          {abstract} + safeExecute()
          {abstract} + waitUntil()
          {abstract} + acquireLock()
          {abstract} + update()
      }

      class SynchronizerImpl{
          + safeExecute()
          + waitUntil()
          + acquireLock()
          + update()
          --
          - _response_time
          - _update_mutex
          - _update
      }

      Synchronizer <|.. SynchronizerImpl

    interface InterruptableThread {
        {abstract} + threadSafeExecute()
        {abstract} + threadWaitUntil()
        {abstract} + isInitializeRequested()
        {abstract} + setInitialized()
        {abstract} + isTerminateRequested()
        {abstract} + setTerminated()
        {abstract} + waitInitializationRequest()
        {static} +run()
    }
    note right: "Initializable/Terminatable thread interface for thread"

    interface InterruptableHost{
        {abstract} + hostSafeExecute()
        {abstract} + hostWaitUntil()
        {abstract} + requestInitialize()
        {abstract} + isInitialized()
        {abstract} + requestTerminate()
        {abstract} + isTerminated()
    }
    note left: "Initialize-able/Terminable thread interface for host"

    class InterruptableImpl{
        + threadSafeExecute()
        + threadWaitUntil()
        + isInitializeRequested()
        + setInitialized()
        + isTerminateRequested()
        + setTerminated()
        + waitInitializationRequest()
        --
        + hostSafeExecute()
        + hostWaitUntil()
        + requestInitialize()
        + isInitialized()
        + requestTerminate()
        + isTerminated()
        --
        - _initialize_requested
        - _initialized
        - _terminate_requested
        - _terminated
        - _last_terminate_check
    }

    InterruptableHost <|.. InterruptableImpl
    InterruptableThread <|.. InterruptableImpl

    Synchronizer <|.. InterruptableHost
    Synchronizer <|.. InterruptableThread
    SynchronizerImpl <|-- InterruptableImpl


    interface AnytimeThread {
        {abstract} + publishFeedback()
        {abstract} + publishResult()
    }

    interface AnytimeHost{
        {abstract} + done()
        {abstract} + waitForFeedback()
        {abstract} + waitForResult()
    }

    class AnytimeImpl{
        + publishFeedback()
        + publishResult()
        --
        + done()
        + waitForFeedback()
        + waitForResult()
        --
        - function<FeedbackType> _feedback_callback
        - function<ResultType> _result_callback
        - queue<FeedbackType> _feedback_queue
        - ResultType _result
    }


    AnytimeHost   <|.. AnytimeImpl
    AnytimeThread <|.. AnytimeImpl

    InterruptableHost <|.. AnytimeHost
    InterruptableThread <|.. AnytimeThread
    InterruptableImpl <|-- AnytimeImpl
  }
}


package threads <<frame>> #DDDDFF {
    namespace Threads {
        interface Action<FeedbackType,ResultType> {
            {abstract} + preempt()
            {abstract} + done()
            {abstract} + waitForFeedback()
            {abstract} + waitForResult()
            --
            + begin()
            + end()
        }

        class ActionImpl<FeedbackType,ResultType>{
            + ActionImpl()
            + ~ActionImpl()
            --
            + preempt()
            + done()
            + waitForFeedback()
            + waitForResult()
            --
            # end_of_most_derived_constructor()
            # beginning_of_most_derived_destructor()
            {abstract} # run() = 0
            --
            - execute_run()
            --
            - AnytimeHost _host_sync
            - AnytimeThread _thread_sync
            - std::thread _thread_handle_sptr
        }

        Action <|.. ActionImpl
        ActionImpl *-- AnytimeHost
        ActionImpl *-- AnytimeThread


        class YieldingActionImpl<FeedbackType,ResultType>{
            + YieldingActionImpl()
            + ~YieldingActionImpl()
            --
            {abstract} # run() = 0
            --
            - run() : Implement ActionImpl.run()
        }

        ActionImpl <|-- YieldingActionImpl
    }
}

@enduml
```
