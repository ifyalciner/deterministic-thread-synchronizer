
# Build the project
# The project is built in the build directory
# The build directory is created if it does not exist

build_env:
	mkdir -p build


build_dev: build_env
	cd build && cmake .. -DDEVELOPER_MODE=true \
											 -DDEBUGGING=true \
											 -DFERGUSON_BUILD_UNIT_TESTS=true \
											 -DBoost_USE_STACKTRACE=true \
											 -DCMAKE_BUILD_TYPE=Debug \
											 -GNinja && ninja

copy_dev_files:
	mkdir -p build
	cp ~/.zshrc build/.zshrc
	cp ~/.zsh_history build/.zsh_history
	cp -r ~/.config build/.config
	rm -rf build/.tmux
	cp -r ~/.tmux build/.tmux
	cp -r ~/.tmux.conf build/.tmux.conf

# tag the docker image as latest
build_base_docker:
	docker build -t ferguson-base:latest -f Dockerfile.base .


# build developer docker image from Dockerfile
# pass current user id as build argument
build_dev_docker: copy_dev_files
	docker build \
			-t ferguson-dev:latest \
			-f Dockerfile.dev \
			--build-arg USER=$(shell whoami) \
			.

			#--build-arg USER=$(shell whoami) \
# Do following
# Use Host network
# open ssh port 22
# read-only map following files/directories
# 	* ~/.gitconfig
# 	* ~/.ssh
# 	* ~/.config
# 	* ~/.zshrc
# 	* ~/.oh-my-zsh
#
#
#
# Login with user dev
# Start shell in /home/dev/app
# Share copy paste buffer with host
	#xhost +local:root
shell:
	docker run \
        -it \
        --rm \
        --network host \
				-e DISPLAY=host.docker.internal:0 \
        -p 22:22 \
        -v ~/.gitconfig:/home/$(shell whoami)/.gitconfig:ro \
        -v ~/.ssh:/home/$(shell whoami)/.ssh:ro \
        -v ~/.oh-my-zsh:/home/$(shell whoami)/.oh-my-zsh:ro \
				-v ~/.p10k.zsh:/home/$(shell whoami)/.p10k.zsh:ro \
        ferguson-dev:latest \
        /bin/zsh -c "cd /home/$(shell whoami)/app && /bin/zsh"


