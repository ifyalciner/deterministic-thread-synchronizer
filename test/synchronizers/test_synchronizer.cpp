// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <iostream>
#include <random>
#include <thread>

#include "../../src/synchronizers/synchronizer_impl.h"
#include <ferguson/synchronizers/synchronizer.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;
using namespace Ferguson::Synchronizer;

class SynchronizerTest: public ::testing::Test {
public:
  void
  SetUp() override {
    max_allowed_delay = TimeUnit(getRandomDelay());
    sync_ptr          = std::make_unique<SynchronizerImpl>(max_allowed_delay);
    GTEST_OUT << "Delay " << max_allowed_delay.count() << "ms" << std::endl;
  }

  void
  TearDown() override {
    max_allowed_delay = TimeUnit(0);
    sync_ptr          = nullptr;
  }

  static uint64_t
  getRandomDelay() {
    // Generate random delay
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint64_t> dist(10, 100);
    return dist(mt);
  }

  std::unique_ptr<SynchronizerImpl> sync_ptr;
  TimeUnit max_allowed_delay{};
};

TEST_F(SynchronizerTest, AcquireLock) {
  auto succ_lock = sync_ptr->acquireLock();
  // Test successful case
  EXPECT_TRUE(succ_lock.first);
  EXPECT_TRUE(succ_lock.second.owns_lock());

  // Now the lock is acquired test return from failure condition
  bool terminate  = false;
  auto term_check = [&]() { return terminate; };
  auto test       = [&]() {
    auto succ_lock2 = sync_ptr->acquireLock(term_check);
    EXPECT_FALSE(succ_lock2.first);
    EXPECT_FALSE(succ_lock2.second.owns_lock());
  };

  std::thread th(test);
  Ferguson::sleepFor(TimeUnit(uint64_t(max_allowed_delay.count() * 0.2)));
  auto start = Clock::now();
  terminate  = true;
  th.join();
  auto finish = Clock::now();
  EXPECT_TRUE(boost::chrono::duration_cast<TimeUnit>(finish - start) <= max_allowed_delay);
}

TEST_F(SynchronizerTest, safeExecute) {
  /* Test racing condition of X number of threads
   * Even though update_delay is larger than 0, on every execution
   * synchronizer will  notify others that _resource is available so total time
   * will not exceed #threads * thread_duration.
   */

  uint64_t counter = 0;
  TimeUnit incrementor_delay(max_allowed_delay - TimeUnit(1));
  auto incrementor = [&]() {
    uint64_t local_counter = counter + 1;
    Ferguson::sleepFor(incrementor_delay);
    counter = local_counter;
  };

  auto thread_routine = [&]() { sync_ptr->safeExecute(incrementor, TimePoint::max(), ALWAYS_FALSE, ALWAYS_TRUE); };

  std::vector<std::thread> threads;
  uint64_t const no_threads = 20;
  auto start                = Clock::now();
  for(uint64_t i = 0; i < no_threads; i++) {
    threads.emplace_back(thread_routine);
  }
  for(auto &t : threads) {
    if(t.joinable()) {
      t.join();
    }
  }
  auto finish = Clock::now();

  EXPECT_EQ(no_threads, counter);

  auto delay = boost::chrono::duration_cast<TimeUnit>(finish - start);
  // Additional 2% time
  auto expected_delay = TimeUnit(int(no_threads * incrementor_delay.count() * 1.02));
  EXPECT_TRUE(delay <= expected_delay) << "Delay " << delay.count() << "ms exceeds the limit " << expected_delay.count()
                                       << "ms";
}

TEST_F(SynchronizerTest, safeExecuteFailCondition) {
  // Success condition hasn't been met so procedure will not be executed
  // We meet with failure condition without notifying the thread but it should
  // still requestTerminate in max_allowed_delay from the moment;
  bool terminate      = false;
  bool success        = false;
  int counter         = 0;
  auto work           = [&]() { counter++; };
  auto succ_condition = [&]() { return success; };
  auto fail_condition = [&]() { return terminate; };
  auto thread_proc    = [&]() { sync_ptr->safeExecute(work, TimePoint::max(), fail_condition, succ_condition); };

  std::thread th(thread_proc);
  auto start = Clock::now();
  terminate  = true;// failure condition is met but no notification
  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  auto delay  = boost::chrono::duration_cast<TimeUnit>(finish - start);

  /* Process terminated in allowed delay without running the procedure */
  EXPECT_TRUE(delay <= max_allowed_delay)
      << "Delay " << delay.count() << "ms exceeds the limit " << max_allowed_delay.count() << "ms";
  EXPECT_EQ(counter, 0);
}

TEST_F(SynchronizerTest, safeExecuteSuccCondition) {
  bool terminate      = false;
  bool success        = false;
  int counter         = 0;
  auto work           = [&]() { counter++; };
  auto succ_condition = [&]() { return success; };
  auto fail_condition = [&]() { return terminate; };
  auto thread_proc    = [&]() { sync_ptr->safeExecute(work, TimePoint::max(), fail_condition, succ_condition); };

  std::thread th(thread_proc);

  auto start = Clock::now();
  success    = true;// success condition is met but no notification

  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  auto delay  = boost::chrono::duration_cast<TimeUnit>(finish - start);

  /* Process run in allowed delay */
  EXPECT_TRUE(delay <= max_allowed_delay)
      << "Delay " << delay.count() << "ms exceeds the limit " << max_allowed_delay.count() << "ms";
  EXPECT_EQ(counter, 1);
}

TEST_F(SynchronizerTest, safeExecuteTimeout) {
  // Acquire lock beforehand so safeExecute cannot acquire it but only return after timeout
  int counter     = 0;
  auto work       = [&]() { counter++; };
  bool return_val = false;

  TimeUnit timeout(max_allowed_delay);
  auto start       = Clock::now();
  auto thread_proc = [&]() {
    // Because wait condition will never be satisfied, this work will never be executed
    return_val = sync_ptr->safeExecute(work, toDeadline(timeout, start), ALWAYS_FALSE, ALWAYS_FALSE);
  };
  sync_ptr->acquireLock();
  std::thread th(thread_proc);
  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  auto delay  = boost::chrono::duration_cast<TimeUnit>(finish - start);

  EXPECT_EQ(return_val, false);
  /* Process terminated in allowed delay without running the procedure */
  EXPECT_TRUE(delay <= timeout) << "Delay " << delay.count() << "ms exceeds the limit " << timeout.count() << "ms";
  EXPECT_EQ(counter, 0);
}

TEST_F(SynchronizerTest, safeExecuteFailConditionUpdate) {
  // Success condition hasn't been met so procedure will not be executed
  // We meet with failure condition without notifying the thread but it should
  // still requestTerminate in max_allowed_delay from the moment;
  bool terminate      = false;
  int counter         = 0;
  auto work           = [&]() { counter++; };
  auto fail_condition = [&]() { return terminate; };
  auto thread_proc    = [&]() { sync_ptr->safeExecute(work, TimePoint::max(), fail_condition, ALWAYS_FALSE); };

  std::thread th(thread_proc);
  auto start = Clock::now();
  terminate  = true;// failure condition is met but no notification
  sync_ptr->update();
  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  auto delay  = boost::chrono::duration_cast<TimeUnit>(finish - start);
  // 2ms is given for thread
  auto allowed_delay = TimeUnit(2);

  /* Process terminated in allowed delay without running the procedure */
  EXPECT_TRUE(delay <= allowed_delay) << "Delay " << delay.count() << "ms exceeds the limit " << allowed_delay.count()
                                      << "ms";
  EXPECT_EQ(counter, 0);
}

TEST_F(SynchronizerTest, safeExecuteSuccConditionUpdate) {
  bool success        = false;
  int counter         = 0;
  auto work           = [&]() { counter++; };
  auto succ_condition = [&]() { return success; };
  auto thread_proc    = [&]() { sync_ptr->safeExecute(work, TimePoint::max(), ALWAYS_FALSE, succ_condition); };

  std::thread th(thread_proc);
  auto start = Clock::now();
  success    = true;// success condition is met but no notification
  sync_ptr->update();
  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  auto delay  = boost::chrono::duration_cast<TimeUnit>(finish - start);
  // 2ms is given for thread
  auto allowed_delay = TimeUnit(2);

  /* Process run in allowed delay */
  EXPECT_TRUE(delay <= allowed_delay) << "Delay " << delay.count() << "ms exceeds the limit " << allowed_delay.count()
                                      << "ms";
  EXPECT_EQ(counter, 1);
}
