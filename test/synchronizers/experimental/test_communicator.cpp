// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <queue>
#include <thread>

#include <ferguson/synchronizers/experimental/communicator.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "
using namespace Ferguson;

struct Input: public Communicator::InputBase {

  explicit Input(int val_): val(val_) {}

  int val;
};
DEFINE_SMARTPTR(Input);

struct Output: public Communicator::OutputBase {
  int val;

  explicit Output(int val_): val(val_) {}
};
DEFINE_SMARTPTR(Output);

class CommunicatorTest: public ::testing::Test {
public:
  void
  SetUp() override {
    max_allowed_delay = TimeUnit(100);
    std::tie(sync_ptr, sync_thread_ptr) =
        Communicator::factory(max_allowed_delay,//
                              [this](const Communicator::OutputBaseCSPtr& output) {
                                output_data.push(std::dynamic_pointer_cast<Output const>(output)->val);
                              });
    GTEST_OUT << "Delay " << max_allowed_delay.count() << "ms" << std::endl;
  }

  void
  TearDown() override {
    output_data       = std::queue<int>();
    max_allowed_delay = TimeUnit(0);
    sync_ptr          = nullptr;
    sync_thread_ptr   = nullptr;
  }

  Communicator::HostSPtr sync_ptr;
  Communicator::ThreadSPtr sync_thread_ptr;
  TimeUnit max_allowed_delay{};
  std::queue<int> output_data;
};

TEST_F(CommunicatorTest, SendNewData) {
  auto message_processor = [](const ::Communicator::InputBaseCSPtr& input) -> ::Communicator::OutputBaseCSPtr {
    InputCSPtr vinput = std::dynamic_pointer_cast<Input const>(input);
    return std::make_shared<Output>(vinput->val + 1);
  };
  auto thread_routine = [capture0 = sync_thread_ptr, message_processor] { Communicator::run(capture0, message_processor); };
  std::thread th(thread_routine);
  sync_ptr->requestInitialize();
  auto start = Clock::now();
  sync_ptr->toThread(std::make_shared<Input>(0));
  sync_ptr->toThread(std::make_shared<Input>(1));
  sync_ptr->toThread(std::make_shared<Input>(2));
  while(sync_thread_ptr->isDataAvailable()) {
    Ferguson::sleepFor(TimeUnit(1));
  }
  auto finish = Clock::now();
  auto delay  = std::chrono::duration_cast<TimeUnit>(finish - start);
  GTEST_OUT << "Processing delay " << delay.count() << "ms" << std::endl;
  // MessageBase processing should be immediate
  EXPECT_TRUE(delay <= TimeUnit(2));

  EXPECT_EQ(output_data.front(), 1);
  output_data.pop();
  EXPECT_EQ(output_data.front(), 2);
  output_data.pop();
  EXPECT_EQ(output_data.front(), 3);
  output_data.pop();
  sync_ptr->requestTerminate();
  if(th.joinable()) {
    th.join();
  }
}
