// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <chrono>
#include <condition_variable>
#include <iostream>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <utility>

#include <ferguson/synchronizers/experimental/yielder.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "
using namespace Ferguson;

struct Input: public Communicator::InputBase {

  explicit Input(int val_): val(val_) {}

  int val;
};
DEFINE_SMARTPTR(Input);

struct Output: public Communicator::OutputBase {
  bool done;
  int val;
  std::string feedback;

  Output(bool done_, int val_, std::string feedback_)
      : done(done_), val(val_), feedback(std::move(std::move(feedback_))) {}
};
DEFINE_SMARTPTR(Output);

class YielderTest: public ::testing::Test {
public:
  void
  SetUp() override {
    max_allowed_delay = TimeUnit(100);
    std::tie(sync_ptr, sync_thread_ptr) =
        ::Yielder::factory(max_allowed_delay,//
                           [this](const ::Communicator::OutputBaseCSPtr &output) {
                             std::unique_lock<std::mutex> lk(output_data_mutex);
                             output_data.push(std::dynamic_pointer_cast<Output const>(output));
                             lk.unlock();
                             output_data_cv.notify_all();
                           });//
    GTEST_OUT << "Delay " << max_allowed_delay.count() << "ms" << std::endl;
  }

  void
  TearDown() override {
    output_data       = std::queue<OutputCSPtr>();
    max_allowed_delay = TimeUnit(0);
    sync_ptr          = nullptr;
    sync_thread_ptr   = nullptr;
  }

  Yielder::HostSPtr sync_ptr;
  Yielder::ThreadSPtr sync_thread_ptr;
  TimeUnit max_allowed_delay{};
  std::mutex output_data_mutex;
  std::condition_variable output_data_cv;
  std::queue<OutputCSPtr> output_data;
};

TEST_F(YielderTest, GetFeedback) {
  auto message_processor = [](const ::Communicator::InputBaseCSPtr &input,
                              ::Yielder::Thread::Coroutine::push_type &sink) {
    InputCSPtr vinput = std::dynamic_pointer_cast<Input const>(input);
    int init_val      = vinput->val;

    for(int i = 0; i < 100; i++) {
      ++init_val;
      std::string feedback = std::string("Processing ") + std::to_string(init_val) + "%";
      sink(std::make_shared<Output const>(false, init_val, feedback));
      Ferguson::sleepFor(TimeUnit(5));
    }
    sink(std::make_shared<Output const>(true, init_val, std::string("Done!")));
  };

  auto thread_routine = [capture0 = sync_thread_ptr, message_processor] {
    ::Yielder::run(capture0, message_processor);
  };
  std::thread th(thread_routine);
  sync_ptr->requestInitialize();
  sync_ptr->toThread(std::make_shared<Input>(0));
  std::unique_lock<std::mutex> lk(output_data_mutex);

  int check = 0;

  for(int i = 0; i < 100; i++) {
    output_data_cv.wait(lk, [this]() { return !output_data.empty(); });
    OutputCSPtr v = output_data.front();
    output_data.pop();
    EXPECT_EQ(v->done, false);
    EXPECT_EQ(v->val, ++check);
    std::string feedback = std::string("Processing ") + std::to_string(check) + "%";
    EXPECT_EQ(v->feedback, feedback);
    GTEST_OUT << v->feedback << std::endl;
  }
  output_data_cv.wait(lk, [this]() { return !output_data.empty(); });
  auto v = std::dynamic_pointer_cast<Output const>(output_data.front());
  output_data.pop();
  EXPECT_EQ(v->done, true);
  EXPECT_EQ(v->val, check);
  EXPECT_EQ(v->feedback, std::string("Done!"));
  GTEST_OUT << v->feedback << std::endl;
  sync_ptr->requestTerminate();
  if(th.joinable()) {
    th.join();
  }
}

TEST_F(YielderTest, EarlyTerminate) {
  auto message_processor = [](const ::Communicator::InputBaseCSPtr &input,
                              Yielder::Thread::Coroutine::push_type &sink) {
    InputCSPtr vinput = std::dynamic_pointer_cast<Input const>(input);
    int init_val      = vinput->val;

    for(int i = 0; i < 100; i++) {
      ++init_val;
      std::string feedback = std::string("Processing ") + std::to_string(init_val) + "%";
      sink(std::make_shared<Output const>(false, init_val, feedback));
      Ferguson::sleepFor(TimeUnit(5));
    }
    sink(std::make_shared<Output const>(true, init_val, std::string("Done!")));
  };

  auto thread_routine = [capture0 = sync_thread_ptr, message_processor] {
    ::Yielder::run(capture0, message_processor);
  };
  std::thread th(thread_routine);
  sync_ptr->requestInitialize();
  sync_ptr->toThread(std::make_shared<Input>(0));
  int check = 0;
  {
    std::unique_lock<std::mutex> lk(output_data_mutex);

    for(int i = 0; i < 50; i++) {
      output_data_cv.wait(lk, [this]() { return !output_data.empty(); });
      OutputCSPtr v = output_data.front();
      output_data.pop();
      EXPECT_EQ(v->done, false);
      EXPECT_EQ(v->val, ++check);
      std::string feedback = std::string("Processing ") + std::to_string(check) + "%";
      EXPECT_EQ(v->feedback, feedback);
      GTEST_OUT << v->feedback << std::endl;
    }
  }
  sync_ptr->requestTerminate();
  Ferguson::sleepFor(TimeUnit(11));
  if(th.joinable()) {
    th.join();
  }
  // Wait for 11ms (2 message generation time)
  // There shouldn't be any more returns
  EXPECT_TRUE(output_data.empty());
}
