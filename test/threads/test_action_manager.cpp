// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <gtest/gtest.h>

#include <memory>
#include <utility>

#include "../../src/threads/action_impl.h"
#include "../../src/threads/action_manager.h"

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;

class MyActionManaged: public Threads::ActionImpl {
public:
  static std::shared_ptr<Action>
  factory(Threads::ActionID parent,                                              //
          int timeout_ms,                                                        //
          std::optional<Function::FeedbackCB> const &feedback_callback = nullptr,//
          std::optional<Function::ResultCB> const &result_callback     = nullptr) {
    return std::shared_ptr<Action>(new MyActionManaged(parent, timeout_ms, feedback_callback, result_callback));
  }

  ~MyActionManaged() override { stopAction(); }

protected:
  MyActionManaged(Threads::ActionID parent,                                                   //
                  int timeout_ms,                                                             //
                  std::optional<Function::FeedbackCB> const &feedback_callback = std::nullopt,//
                  std::optional<Function::ResultCB> const &result_callback     = std::nullopt)
      : Threads::ActionImpl(parent,              //
                            "ActionManagerTest", //
                            GET_TYPE(),          //
                            TimeUnit(timeout_ms),//
                            feedback_callback,   //
                            result_callback) {
    runAction();
  }

  void
  run(Function::ThreadSPtr thread_sptr) override {}
};

TEST(ActionManagerTest, ActionManager) {
  auto act_0 = MyActionManaged::factory(Threads::Action::root(), 10);
  auto act_1 = MyActionManaged::factory(Threads::ActionID(act_0->id()), 10);
  auto act_2 = MyActionManaged::factory(Threads::ActionID(act_0->id()), 10);
  auto act_3 = MyActionManaged::factory(Threads::ActionID(act_1->id()), 10);
  auto act_4 = MyActionManaged::factory(Threads::ActionID(act_1->id()), 10);
  auto act_5 = MyActionManaged::factory(Threads::ActionID(act_2->id()), 10);
  auto act_6 = MyActionManaged::factory(Threads::ActionID(act_2->id()), 10);

  // Note for these literal values to be true, test_action_manager.cpp should come before all other action tests, since
  // all google tests are run on same process and id is global variable.
  EXPECT_EQ(act_0->id(), 1);
  EXPECT_EQ(act_1->id(), 2);
  EXPECT_EQ(act_2->id(), 3);
  EXPECT_EQ(act_3->id(), 4);
  EXPECT_EQ(act_4->id(), 5);
  EXPECT_EQ(act_5->id(), 6);
  EXPECT_EQ(act_6->id(), 7);

  EXPECT_EQ(act_0->parent(), Threads::Action::root().id);
  EXPECT_EQ(act_1->parent(), act_0->id());
  EXPECT_EQ(act_2->parent(), act_0->id());
  EXPECT_EQ(act_3->parent(), act_1->id());
  EXPECT_EQ(act_4->parent(), act_1->id());
  EXPECT_EQ(act_5->parent(), act_2->id());
  EXPECT_EQ(act_6->parent(), act_2->id());

  auto next_id = Threads::ActionManager::getID();
  // There are already 7 actions, next id should be 8. IDs start from 1
  EXPECT_EQ(next_id, 8);

  auto manager = Threads::ActionManager::get();

  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_0.get()));
  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_1.get()));
  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_2.get()));
  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_3.get()));
  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_4.get()));
  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_5.get()));
  manager->addAction(dynamic_cast<Threads::ActionImpl const *>(act_6.get()));

  // Indirectly test member variables
  manager->removeAction(act_0->id());
  manager->removeAction(act_1->id());
  manager->removeAction(act_2->id());
  manager->removeAction(act_3->id());
  manager->removeAction(act_4->id());
  manager->removeAction(act_5->id());
  manager->removeAction(act_6->id());

  manager->removeAction(act_0->id());
  manager->removeAction(act_1->id());
  manager->removeAction(act_2->id());
  manager->removeAction(act_3->id());
  manager->removeAction(act_4->id());
  manager->removeAction(act_5->id());
  manager->removeAction(act_6->id());
}