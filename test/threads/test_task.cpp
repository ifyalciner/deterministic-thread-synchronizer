// MIT License
//
// Copyright (c) 2024 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <gtest/gtest.h>

#include <memory>
#include <sstream>
#include <thread>
#include <utility>

#include <ferguson/threads/task.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;
using namespace Ferguson::Threads;

class ValidTask {
public:
  explicit ValidTask(int x): _x(x) {}

  void
  operator()(std::function<void(int const &)> const &product_callback) {
    GTEST_OUT << "Valid TaskBase" << std::endl;
    product_callback(_x + 1);
  }

private:
  int _x;
};

class ValidTask2 {
public:
  explicit ValidTask2(int x): _x(x) {}

  void
  operator()(std::function<void(int const &)> const &product_callback) const {
    GTEST_OUT << "Valid TaskBase" << std::endl;
    product_callback(_x + 1);
  }

private:
  int _x;
};

class ValidTask3 {
public:
  explicit ValidTask3(int x): _x(x) {}

  void
  operator()(std::function<void(int)> const &product_callback) const {
    GTEST_OUT << "Valid TaskBase" << std::endl;
    product_callback(_x + 1);
  }

private:
  int _x;
};

TEST(Task, FunctorConstructor) {
  GTEST_OUT << "TaskBase Constructor Test" << std::endl;
  TaskObject<ValidTask> task(1);
  TaskObject<ValidTask2> task2(1);
  TaskObject<ValidTask3> task3(1);
}

TEST(Task, Functions) {
  GTEST_OUT << "TaskBase Constructor Test" << std::endl;
  const int a                = 1;
  auto lambda_test_function  = [](std::function<void(int const &)> const &product_callback) {};
  auto lambda_test_function2 = [b = a](std::function<void(int const &)> const &product_callback) {(void)b;};
  auto lambda_test_function3 = [&a](std::function<void(int const &)> const product_callback) {(void)a;};
  auto lambda_test_function4 = [a](std::function<void(int const &)> product_callback) { (void)a; };
  // convert lambda to std::function
  auto function_test_function = std::function<void(std::function<void(int const &)>)>(lambda_test_function);

  TaskFunction task(lambda_test_function);
  TaskFunction task2(lambda_test_function2);
  TaskFunction task3(lambda_test_function3);
  TaskFunction task4(lambda_test_function4);
  TaskFunction taskf(function_test_function);
}
