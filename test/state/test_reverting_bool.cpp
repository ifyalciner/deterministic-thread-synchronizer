// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <gtest/gtest.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <thread>

#include <ferguson/state/reverting_bool.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson::State;

TEST(RevertingBoolTest, MainFunctionality) {
  auto delay            = Ferguson::TimeUnit(400);
  auto activation_delay = Ferguson::TimeUnit(50);
  // In this test DelayedBool should pretend exactly like bool
  auto db = RevertingBool::factory(false, delay, activation_delay);
  EXPECT_FALSE(*db);
  *db                = true;
  auto activation_dl = Ferguson::toDeadline(activation_delay, Ferguson::Clock::now());
  auto dl            = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  Ferguson::sleepFor(activation_delay / 2);
  // Still should be false because less than activation time passed
  EXPECT_FALSE(*db);
  Ferguson::sleepUntil(activation_dl);
  // Now it should be true since it is activated
  EXPECT_TRUE(*db);
  Ferguson::sleepUntil(dl);
  // Now it will be false due to reversion
  EXPECT_FALSE(*db);
}

TEST(RevertingBoolTest, JitterTest) {
  auto delay            = Ferguson::TimeUnit(400);
  auto activation_delay = Ferguson::TimeUnit(50);
  // In this test DelayedBool should pretend exactly like bool
  auto db = RevertingBool::factory(false, delay, activation_delay);
  EXPECT_FALSE(*db);
  *db = true;
  EXPECT_FALSE(*db);
  Ferguson::sleepFor(activation_delay / 2);
  // Still false
  EXPECT_FALSE(*db);
  // This clears the jitter signal
  *db = false;
  EXPECT_FALSE(*db);
  // Even after waiting enough for activation we should still be false
  Ferguson::sleepFor(activation_delay);
  EXPECT_FALSE(*db);
}

TEST(RevertingBoolTest, JitterTest2) {
  auto delay            = Ferguson::TimeUnit(400);
  auto activation_delay = Ferguson::TimeUnit(50);
  // In this test DelayedBool should pretend exactly like bool
  auto db = RevertingBool::factory(false, delay, activation_delay);
  EXPECT_FALSE(*db);
  *db                    = true;
  auto revert_deadline_1 = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  EXPECT_LE(abs(db->revertingDeadline() - revert_deadline_1), Ferguson::TimeUnit(2));
  // It will be False until activation
  EXPECT_FALSE(*db);
  Ferguson::sleepFor(activation_delay);
  // Now it should be True
  EXPECT_TRUE(*db);
  *db = false;
  // Still true
  EXPECT_TRUE(*db);
  Ferguson::sleepFor(activation_delay / 3);
  // Still true
  EXPECT_TRUE(*db);
  *db                    = true;// This should clear up the deadline from most recent false
  auto revert_deadline_2 = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  EXPECT_LE(abs(db->revertingDeadline() - revert_deadline_2), Ferguson::TimeUnit(2));
  Ferguson::sleepFor(activation_delay);
  EXPECT_TRUE(*db);
  // After waiting for suitable amount of time, we are reverting now
  Ferguson::sleepUntil(revert_deadline_2);
  EXPECT_FALSE(*db);
}
