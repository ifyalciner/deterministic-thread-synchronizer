// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <gtest/gtest.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <thread>

#include <ferguson/impl/state/state_impl.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;
using namespace Ferguson::State;

TEST(State, StateFactory) {
  EXPECT_NO_THROW(StateSpace::factory({{"var1", 2}, {"var2", 2}}));
  // Warning this is an issue of std::map where second identical key is ignored
  // Following will yield a namespace with single dimension `var1` which has range of 2 values
  EXPECT_NO_THROW(StateSpace::factory({{"var1", 2}, {"var1", 2}}));
}

TEST(State, SpaceMixMatch) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto sf2 = StateSpace::factory({{"var1", 3},//
                                  {"var2", 2},//
                                  {"var3", 2}});

  auto s  = sf->state({{"var1", 1}});
  auto s2 = sf2->state({{"var1", 1}});
  EXPECT_THROW(s & s2, std::invalid_argument);
}

TEST(State, StateFactoryState) {
  auto sf = StateSpace::factory({{"var1", 2},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  // Default values for state is 0
  auto sx = sf->state({{"var1", 0}, {"var2", 0}, {"var3", 0}});
  auto s1 = sf->state();
  auto s2 = sf->state({{"var1", 0}});
  auto s3 = sf->state({{"var1", 0}, {"var2", 0}});
  EXPECT_EQ(sx, s1);
  EXPECT_EQ(sx, s2);
  EXPECT_EQ(sx, s3);
}

TEST(State, StateFactorySuperstate) {
  auto sf = StateSpace::factory({{"var1", 2},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  // Default values for state is 0
  auto sx = sf->super_state({{"var1", {0, 1}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto s1 = sf->super_state();
  auto s2 = sf->super_state({{"var1", {0, 1}}});
  auto s3 = sf->super_state({{"var1", {0, 1}}, {"var2", {0, 1}}});
  EXPECT_EQ(sx, s1);
  EXPECT_EQ(sx, s2);
  EXPECT_EQ(sx, s3);
}

TEST(State, SuperStatePowerSuperState) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1 = sf->super_state({{"var2", {0}}});
  auto ss2 = sf->super_state({{"var2", {1}}});
  EXPECT_TRUE((ss1 | ss2) == sf->power_super_state());
}

TEST(State, StateFactoryEmptySuperState) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1 = sf->super_state({{"var2", {0}}});
  auto ss2 = sf->super_state({{"var2", {1}}});
  EXPECT_TRUE((ss1 & ss2) == sf->empty_super_state());
}

TEST(State, AnyState_space) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});
  auto s  = sf->empty_super_state();
  EXPECT_EQ(sf, s.space());
}

TEST(State, AnyState_is_subset_of) {
  auto sf  = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});
  auto ssp = sf->power_super_state();
  auto sse = sf->empty_super_state();
  auto s   = sf->state();

  EXPECT_TRUE(ssp.is_subset_of(ssp));
  EXPECT_TRUE(sse.is_subset_of(ssp));
  EXPECT_FALSE(ssp.is_subset_of(sse));
  EXPECT_TRUE(s.is_subset_of(ssp));
}

TEST(State, AnyState_is_proper_subset_of) {
  auto sf  = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});
  auto ssp = sf->power_super_state();
  auto sse = sf->empty_super_state();
  auto s   = sf->state();

  EXPECT_TRUE(sse.is_proper_subset_of(ssp));
  EXPECT_FALSE(ssp.is_proper_subset_of(ssp));
  EXPECT_TRUE(s.is_proper_subset_of(ssp));
  EXPECT_FALSE(ssp.is_proper_subset_of(s));
}

TEST(State, AnyState_operator_equal) {
  auto sf        = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});
  auto ss1       = sf->power_super_state();
  auto const ss2 = sf->power_super_state();
  auto ss3       = sf->super_state();
  auto ss4       = sf->super_state({{"var1", {0, 1, 2}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto ss5       = sf->empty_super_state();
  auto ss6       = sf->super_state({{"var1", {1, 2}}, {"var2", {0, 1}}, {"var3", {0, 1}}});

  auto s1       = sf->state({{"var1", 0}, {"var2", 1}, {"var3", 0}});
  auto const s2 = sf->state({{"var1", 0}, {"var2", 1}, {"var3", 0}});
  auto s3       = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 0}});

  EXPECT_TRUE(ss1 == (ss1));
  EXPECT_FALSE(ss1 != (ss1));
  EXPECT_TRUE(ss1 == ss1);
  EXPECT_TRUE(ss1 == ss2);
  EXPECT_TRUE(ss1 == ss3);
  EXPECT_TRUE(ss1 == ss4);

  EXPECT_FALSE(ss1 == ss5);
  EXPECT_TRUE(ss1 != ss5);
  EXPECT_FALSE(ss1 == ss6);
  EXPECT_TRUE(ss1 != ss6);

  EXPECT_TRUE(s1 == s1);
  EXPECT_FALSE(s1 != s1);
  EXPECT_TRUE(s1 == s1);
  EXPECT_TRUE(s1 == s2);

  EXPECT_FALSE(s1 == s3);
  EXPECT_TRUE(s1 != s3);
}

TEST(State, AnyState_operator_or) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1       = sf->super_state({{"var1", {0}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto const ss2 = sf->super_state({{"var1", {1}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto ss3       = sf->super_state({{"var1", {2}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto ss4       = sf->power_super_state();

  EXPECT_TRUE((ss1 | ss2 | ss3) == ss4);

  // Test combination of states and a super state
  auto s1       = sf->state({{"var1", 0}});
  auto const s2 = sf->state({{"var1", 1}});
  auto s3       = sf->state({{"var1", 2}});
  auto ss5      = sf->super_state({{"var1", {0, 1, 2}}, {"var2", {0}}, {"var3", {0}}});
  EXPECT_TRUE((s1 | s2 | s3) == ss5);
}

TEST(State, AnyState_operator_and) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1       = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {1}}});
  auto const ss2 = sf->super_state({{"var1", {1, 2}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto s1        = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto const s2  = sf->state({{"var1", 0}, {"var2", 1}, {"var3", 1}});
  auto sse       = sf->empty_super_state();

  EXPECT_TRUE((ss1 & ss2) == s1);
  EXPECT_TRUE((s1 & s2) == sse);
}

TEST(State, AnyState_operator_xor) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1       = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto const ss2 = sf->super_state({{"var1", {1, 2}}, {"var2", {1}}, {"var3", {0, 1}}});
  // Followings are to check ss3 and ss5 should be in ss1 ^ ss2 while ss4 shouldn't
  auto const ss3 = sf->super_state({{"var1", {0}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto const ss4 = sf->super_state({{"var1", {1}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto const ss5 = sf->super_state({{"var1", {2}}, {"var2", {1}}, {"var3", {0, 1}}});

  auto s1       = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto const s2 = sf->state({{"var1", 0}, {"var2", 1}, {"var3", 1}});
  auto sse      = sf->empty_super_state();

  EXPECT_TRUE(ss3.is_subset_of((ss1 ^ ss2)));
  EXPECT_FALSE(ss4.is_subset_of((ss1 ^ ss2)));
  EXPECT_TRUE(ss5.is_subset_of((ss1 ^ ss2)));
  EXPECT_TRUE(s1.is_subset_of(s1 ^ s2));
  EXPECT_TRUE(s2.is_subset_of(s1 ^ s2));
}

TEST(State, AnyState_operator_negate) {
  auto sf = StateSpace::factory({{"var1", 2},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto s1        = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto const ss1 = sf->super_state({{"var1", {0}}});
  auto const ss2 = sf->super_state({{"var2", {0}}});
  auto const ss3 = sf->super_state({{"var3", {0}}});

  EXPECT_EQ(~s1, ss1 | ss2 | ss3);
  EXPECT_FALSE((~s1).is_subset_of(ss1 & ss2 & ss3));
}

TEST(State, AnyState_operator_stream) {}

TEST(State, AnyState_sample) {
  auto sf = StateSpace::factory({{"var1", 2},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1 = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto s1  = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});

  for(int i = 0; i < 100; i++) {
    auto ss = ss1.sample();
    EXPECT_TRUE(ss.is_subset_of(ss1));
    auto s = s1.sample();
    EXPECT_TRUE(s.is_subset_of(s1));
  }
}

TEST(State, SuperState_operator_or_equal) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1       = sf->super_state({{"var1", {0}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto const ss2 = sf->super_state({{"var1", {1}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto ss3       = sf->super_state({{"var1", {2}}, {"var2", {0, 1}}, {"var3", {0, 1}}});
  auto ss5       = sf->empty_super_state();
  auto ss4       = sf->power_super_state();
  ss5 |= ss1;
  ss5 |= ss2;
  ss5 |= ss3;

  EXPECT_TRUE(ss5 == (ss1 | ss2 | ss3));

  // Test combination of states and a super state
  auto s1       = sf->state({{"var1", 0}});
  auto const s2 = sf->state({{"var1", 1}});
  auto s3       = sf->state({{"var1", 2}});
  auto ss6      = sf->super_state({{"var1", {0, 1, 2}}, {"var2", {0}}, {"var3", {0}}});
  auto ss7      = sf->empty_super_state();
  ss7 |= s1;
  ss7 |= s2;
  ss7 |= s3;
  EXPECT_TRUE(ss7 == (s1 | s2 | s3));
}

TEST(State, SuperState_operator_and_equal) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1       = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {1}}});
  auto const ss2 = sf->super_state({{"var1", {1, 2}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto s1        = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto const s2  = sf->state({{"var1", 0}, {"var2", 1}, {"var3", 1}});
  auto ssf       = sf->power_super_state();
  ssf &= ss1;
  ssf &= ss2;
  EXPECT_TRUE(ssf == (ss1 & ss2));
  ssf &= s2;
  EXPECT_TRUE(ssf == (ss1 & ss2 & s2));
}

TEST(State, SuperState_operator_xor_equal) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  {
    auto ss1       = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
    auto const ss2 = sf->super_state({{"var1", {1, 2}}, {"var2", {1}}, {"var3", {0, 1}}});
    auto sse       = sf->empty_super_state() | ss1;
    sse ^= ss2;
    EXPECT_EQ(sse, (ss1 ^ ss2));
  }

  {
    auto s1       = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
    auto const s2 = sf->state({{"var1", 0}, {"var2", 1}, {"var3", 1}});
    auto sse      = sf->empty_super_state() | s1;
    sse ^= s2;
    EXPECT_EQ(sse, (s1 ^ s2));
  }
}

TEST(State, SuperState_copy_constructor) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1 = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
  const SuperState& ss2(ss1);
  EXPECT_EQ(ss1, ss2);
}

TEST(State, SuperState_move_constructor) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1 = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto ss3 = ss1;
  SuperState ss2(std::move(ss1));
  EXPECT_EQ(ss2, ss3);
  EXPECT_TRUE(ss1.pimpl() == nullptr);
}

TEST(State, SuperState_operator_assing) {

  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1 = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto ss2 = sf->super_state({{"var1", {1, 2}}, {"var2", {1}}, {"var3", {0, 1}}});
  ss1      = ss2;
  EXPECT_EQ(ss1, ss2);
}

TEST(State, SuperState_operator_move) {

  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto ss1    = sf->super_state({{"var1", {0, 1}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto ss2    = sf->super_state({{"var1", {1, 2}}, {"var2", {1}}, {"var3", {0, 1}}});
  auto ss3    = ss2;
  ss1.operator=(std::move(ss2));
  EXPECT_EQ(ss1, ss3);
  EXPECT_TRUE(ss2.pimpl() == nullptr);
}

TEST(State, State) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});
  // Wrong dimension name
  EXPECT_THROW(sf->state({{"var4", 3}}), std::invalid_argument);
  // Wrong dimension size
  EXPECT_THROW(sf->state({{"var1", 4}}), std::invalid_argument);

  auto s = sf->state({{"var1", 1}});
  EXPECT_EQ(s.at("var1"), 1);
  EXPECT_EQ(s.at("var2"), 0);
  EXPECT_EQ(s.at("var3"), 0);

  auto s2 = sf->state({{"var1", 2}, {"var2", 1}});
  EXPECT_EQ(s2.at("var1"), 2);
  EXPECT_EQ(s2.at("var2"), 1);
  EXPECT_EQ(s2.at("var3"), 0);

  auto s3 = sf->state({{"var3", 1}, {"var1", 0}});
  EXPECT_EQ(s3.at("var1"), 0);
  EXPECT_EQ(s3.at("var2"), 0);
  EXPECT_EQ(s3.at("var3"), 1);

  // Empty map item {} translates to {"",0} by std::map
  EXPECT_THROW(sf->state({{}}), std::invalid_argument);

  auto ss = s | s2;
  EXPECT_TRUE((ss & s) == s);
  EXPECT_FALSE((ss & s) == s3);

  s.at("var1") = 0;
  EXPECT_EQ(s.at("var1"), 0);
  s.at("var1") = 1;
  EXPECT_EQ(s.at("var1"), 1);
  s.at("var1") = 2;
  EXPECT_EQ(s.at("var1"), 2);
  EXPECT_THROW(s.at("var1") = 3, std::invalid_argument);
  EXPECT_THROW(s.at("var1") = -1, std::invalid_argument);

  s["var1"] = 0;
  EXPECT_EQ(s["var1"], 0);
  s["var1"] = 1;
  EXPECT_EQ(s["var1"], 1);
  s["var1"] = 2;
  EXPECT_EQ(s["var1"], 2);
  EXPECT_THROW(s["var1"] = 3, std::invalid_argument);
  EXPECT_THROW(s["var1"] = -1, std::invalid_argument);
}

TEST(State, State_copy_constructor) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto s1 = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  const Ferguson::State::State& s2(s1);
  EXPECT_EQ(s1, s2);
}

TEST(State, State_move_constructor) {
  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto s1 = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto s3 = s1;
  Ferguson::State::State s2(std::move(s1));
  EXPECT_EQ(s2, s3);
  EXPECT_TRUE(s1.pimpl() == nullptr);
}

TEST(State, State_operator_assing) {

  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto s1       = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto const s2 = sf->state({{"var1", 2}, {"var2", 1}, {"var3", 1}});
  s1            = s2;
  EXPECT_EQ(s1, s2);
}

TEST(State, State_operator_move) {

  auto sf = StateSpace::factory({{"var1", 3},//
                                 {"var2", 2},//
                                 {"var3", 2}});

  auto s1 = sf->state({{"var1", 1}, {"var2", 1}, {"var3", 1}});
  auto s2 = sf->state({{"var1", 2}, {"var2", 1}, {"var3", 1}});
  auto s3 = s2;
  s1      = std::move(s2);
  EXPECT_EQ(s1, s3);
  EXPECT_TRUE(s2.pimpl() == nullptr);
}
