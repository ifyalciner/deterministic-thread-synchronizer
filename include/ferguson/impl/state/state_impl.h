// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FERGUSON_INCLUDE_FERGUSON_IMPL_STATE_STATE_IMPL_H
#define FERGUSON_INCLUDE_FERGUSON_IMPL_STATE_STATE_IMPL_H

/* Local */
#include <ferguson/state/state.h>
#include <ferguson/utils.h>

/* CPP */
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

/* Boost */
#include <boost/dynamic_bitset.hpp>
#include <boost/multiprecision/cpp_int.hpp>

namespace Ferguson::State {

class StateSpaceImpl;
DEFINE_SMARTPTR(StateSpaceImpl);

struct StateSpaceDescription {
  uint64_t const no_dimensions;
  std::vector<std::string> const index_to_name;
  std::unordered_map<std::string, uint64_t> const name_to_index;
  std::vector<uint64_t> const dimension_size;
  std::vector<uint64_t> const dimension_step_size;
  uint64_t const space_size;

  using ValType = boost::dynamic_bitset<uint64_t>;

  ValType
  get_zero_value() const;

  ValType
  get_bitset(ValType const &values) const;

  /**
   * @brief
   * @param values
   * @return
   * @warning Assuming values is complete map
   */
  ValType
  get_bitset(std::map<std::string, std::set<uint64_t>> const &values) const;

  /**
   * @brief
   * @param values
   * @return
   * @warning Assuming values is complete map
   */
  ValType
  get_bitset(std::map<std::string, uint64_t> const &values) const;

  std::map<std::string, uint64_t>
  get_map(uint64_t state_index) const;

  static std::map<std::string, uint64_t>
  get_map(std::map<std::string, uint64_t> map);

  /**
   * @brief  Missing dimensions are assigned to zero.
   * @param map
   */
  std::map<std::string, uint64_t> &
  complete_map(std::map<std::string, uint64_t> &map) const;

  /**
   * @brief Missing dimensions are assigned to set of all valid values.
   * @param map
   */
  std::map<std::string, std::set<uint64_t>> &
  complete_map(std::map<std::string, std::set<uint64_t>> &map) const;

  void
  check_dimension(std::string const &) const;
  void
  check_range(std::string const &dimension, uint64_t value) const;
  void
  check_range(std::string const &dimension, std::set<uint64_t> const &value) const;

  template<typename T>
  void
  check_health(std::map<std::string, T> const &values) const {
    //if(values.empty()) {
    //  std::stringstream ss;
    //  ss << "Error: Map cannot be empty " << std::endl;
    //  throw std::invalid_argument(ss.str());
    //}
    for(auto const &it : values) {
      check_dimension(it.first);
      check_range(it.first, it.second);
    }
  }

  /**
   * @brief Returns the index of sampled state
   * @param v
   * @return
   */
  static uint64_t
  sample(ValType const &v);

private:
  void
  get_bitset_inner(std::map<std::string, std::set<uint64_t>> const &values,// Settled values
                   StateSpaceDescription::ValType &val,                    //
                   uint64_t cur_index = 0,                                 //
                   uint64_t index     = 0) const;
};
DEFINE_SMARTPTR(StateSpaceDescription);

/*--------------------------------------------------------------------------------------------------------------------*/

class StateSpaceImpl: public virtual StateSpace, public std::enable_shared_from_this<StateSpaceImpl> {
public:
  /**
   * @brief
   * @param state_dimensions: A map of super_state variable name and range size
   */
  explicit StateSpaceImpl(std::map<std::string, uint64_t> const &dimensions);
  virtual ~StateSpaceImpl() = default;

  /**
   * @brief Missing dimensions are assigned to zero value.
   * @param values
   * @return
   */
  State
  state(std::map<std::string, uint64_t> values) const final;

  /**
   * @brief Missing dimensions are assigned to set of all valid values.
   * @param values
   * @return
   */
  SuperState
  super_state(std::map<std::string, std::set<uint64_t>> values) const final;

  SuperState
  empty_super_state() const final;

  SuperState
  power_super_state() const final;

  StateSpaceDescriptionCSPtr
  desc() const;

private:
  StateSpaceDescriptionCSPtr _space_desc;
};

/*--------------------------------------------------------------------------------------------------------------------*/
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

struct AnyStateImpl: public std::enable_shared_from_this<AnyStateImpl> {
  StateSpaceCSPtr
  space() const;

  bool
  is_subset_of(AnyStateImplCSPtr const &s) const;

  bool
  is_proper_subset_of(AnyStateImplCSPtr const &s) const;

  bool
  operator==(AnyStateImplCSPtr const &s) const;

  bool
  operator!=(AnyStateImplCSPtr const &s) const;

  SuperStateImplSPtr
  operator|(AnyStateImplCSPtr const &s) const;

  SuperStateImplSPtr
  operator&(AnyStateImplCSPtr const &s) const;

  SuperStateImplSPtr
  operator^(AnyStateImplCSPtr const &s) const;

  SuperStateImplSPtr
  operator~() const;

  std::ostream &
  operator<<(std::ostream &o) const;

  virtual StateSpaceDescription::ValType
  val() const = 0;

  StateSpaceDescriptionCSPtr
  desc() const;

  virtual AnyStateImplSPtr
  copy() = 0;

  StateImplSPtr
  sample() const;

protected:
  /** Never should be used to create object of AnyState Type */
  explicit AnyStateImpl(StateSpaceImplCSPtr space);

  StateSpaceImplCSPtr const _space;
};

/*--------------------------------------------------------------------------------------------------------------------*/

struct SuperStateImpl: public AnyStateImpl {
  virtual ~SuperStateImpl() = default;

  SuperStateImplSPtr
  operator|=(AnyStateImplCSPtr const &s);

  SuperStateImplSPtr
  operator&=(AnyStateImplCSPtr const &s);

  SuperStateImplSPtr
  operator^=(AnyStateImplCSPtr const &s);

  void
  set(std::map<std::string, std::set<uint64_t>> values);

  StateSpaceDescription::ValType
  val() const final;

  std::vector<StateImplSPtr>
  get_states() const;

  AnyStateImplSPtr
  copy() final;

  template<typename... Args>
  static std::shared_ptr<SuperStateImpl>
  factory(Args... args) {
    return std::shared_ptr<SuperStateImpl>(new SuperStateImpl(args...));
  }

  static SuperStateImplSPtr
  factory(SuperStateImpl const &s);

  static SuperStateImplSPtr
  factory(SuperStateImpl &&s);

  SuperStateImplSPtr
  operator=(SuperStateImpl const &s);

  SuperStateImplSPtr
  operator=(SuperStateImpl &&s) noexcept;

private:
  /* Illegal */
  using AnyStateImpl::operator=;

private:
  /** WARNING CONSTRUCTORS ARE NOT MEANT TO BE USED */

  SuperStateImpl(SuperStateImpl const &s);
  SuperStateImpl(SuperStateImpl &&s) noexcept;

  template<typename T>
  SuperStateImpl(StateSpaceImplCSPtr space,//
                 T values)
      : AnyStateImpl(space),//
        _val(_space->desc()->get_bitset(values)) {}

  StateSpaceDescription::ValType _val;
};

/*--------------------------------------------------------------------------------------------------------------------*/

struct StateImpl: public AnyStateImpl {
  virtual ~StateImpl() = default;

  void
  set(std::string const &dimension, uint64_t value);

  State::ValProxy
  at(std::string const &dimension);

  uint64_t
  at(std::string const &dimension) const;

  StateSpaceDescription::ValType
  val() const final;

  AnyStateImplSPtr
  copy() final;
  // void
  //set_val(StateSpaceDescription::ValType v) ;

  template<typename... Args>
  static std::shared_ptr<StateImpl>
  factory(Args... args) {
    return std::shared_ptr<StateImpl>(new StateImpl(args...));
  }

  static StateImplSPtr
  factory(StateImpl const &s);

  static StateImplSPtr
  factory(StateImpl &&s);

  StateImplSPtr
  operator=(StateImpl const &s);

  StateImplSPtr
  operator=(StateImpl &&s) noexcept;

private:
  /* Illegal */
  using AnyStateImpl::operator=;

private:
  /** WARNING CONSTRUCTORS ARE NOT MEANT TO BE USED */
  StateImpl(StateImpl const &s);
  StateImpl(StateImpl &&s) noexcept;

  template<typename T>
  StateImpl(StateSpaceImplCSPtr space,//
            T values)
      : AnyStateImpl(space),//
        _val(_space->desc()->get_map(values)) {}

  std::map<std::string, uint64_t> _val;
};

}// namespace Ferguson::State

#endif//FERGUSON_INCLUDE_FERGUSON_IMPL_STATE_STATE_IMPL_H
