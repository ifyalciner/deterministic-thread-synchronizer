// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FERGUSON_INCLUDE_FERGUSON_STATE_REVERTING_BOOL_H
#define FERGUSON_INCLUDE_FERGUSON_STATE_REVERTING_BOOL_H

#include <memory>

#include <ferguson/time.h>
#include <ferguson/utils.h>

namespace Ferguson::State {

class RevertingBool {
public:
  /**
   * @brief
   * @param default_value : Value the boolean will be reset after `reverting_delay` without any rewriting
   * @param reverting_delay : After the last assignment to the boolean, it will be reset to `default_value` after this delay.
   * @param activation_delay : Any change shorter than this delay will be ignored.
   * @return
   */
  static std::shared_ptr<RevertingBool>
  factory(bool default_value,                //
          Ferguson::TimeUnit reverting_delay,//
          Ferguson::TimeUnit activation_delay = Ferguson::TimeUnit(0));

  /**
   * @brief  Set boolean value to x immediately.
   * @param x
   * @return Current boolean value.
   */
  virtual RevertingBool &
  operator=(bool x) = 0;

  /**
   * @brief Get current value. If value `!default_value` hasn't been re-set in `reverting_delay` duration, it will return `default_value`.
   * @return Current boolean value
   */
  virtual
  operator bool() const = 0;

  [[nodiscard]] virtual Ferguson::TimePoint
  revertingDeadline() const = 0;
};

}// namespace Ferguson::State

#endif//FERGUSON_INCLUDE_FERGUSON_STATE_REVERTING_BOOL_H
