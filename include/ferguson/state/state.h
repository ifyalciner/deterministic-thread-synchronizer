// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FERGUSON_INCLUDE_FERGUSON_STATE_STATE_H
#define FERGUSON_INCLUDE_FERGUSON_STATE_STATE_H

/* Local */
#include <ferguson/utils.h>

/* CPP */
#include <map>
#include <memory>
#include <set>
#include <string>
#include <type_traits>
#include <vector>

namespace Ferguson::State {

// Forward declarations
struct AnyStateImpl;
DEFINE_SMARTPTR(AnyStateImpl);

struct SuperStateImpl;
DEFINE_SMARTPTR(SuperStateImpl);

struct StateImpl;
DEFINE_SMARTPTR(StateImpl);

struct StateSpace;
DEFINE_SMARTPTR(StateSpace);

struct State;
struct SuperState;

struct AnyState {
  [[nodiscard]] StateSpaceCSPtr
  space() const;

  [[nodiscard]] bool
  is_subset_of(AnyState const &s) const;

  [[nodiscard]] bool
  is_proper_subset_of(AnyState const &s) const;

  bool
  operator==(AnyState const &s) const;

  bool
  operator!=(AnyState const &s) const;

  SuperState
  operator|(AnyState const &s) const;

  SuperState
  operator&(AnyState const &s) const;

  SuperState
  operator^(AnyState const &s) const;

  SuperState
  operator~() const;

  std::ostream &
  operator<<(std::ostream &o) const;

  [[nodiscard]] State
  sample() const;

  [[nodiscard]] AnyStateImplCSPtr
  pimpl() const;

  AnyState(AnyState const &);
  AnyState(AnyState &&) noexcept;
  AnyState &
  operator=(AnyState const &);
  AnyState &
  operator=(AnyState &&) noexcept;

protected:
  explicit AnyState(AnyStateImplSPtr pimpl);
  AnyStateImplSPtr _pimpl;
};

struct SuperState: public AnyState {
  SuperState
  operator|=(AnyState const &s);

  SuperState
  operator&=(AnyState const &s);

  SuperState
  operator^=(AnyState const &s);

  void
  set(std::map<std::string, std::set<uint64_t>> const &values);

  [[nodiscard]] std::vector<State>
  get_states() const;

  SuperState(SuperState const &s);
  SuperState(SuperState &&s) noexcept;
  SuperState &
  operator=(SuperState const &s);
  SuperState &
  operator=(SuperState &&s) noexcept;

  explicit SuperState(AnyStateImplSPtr pimpl);
};

struct State: public AnyState {
  /**
   * @brief Validates values assigned
   */
  struct ValProxy {
    operator uint64_t() const;

    ValProxy &
    operator=(uint64_t val);

    ValProxy(StateImplSPtr s, std::string dimension);

  private:
    StateImplSPtr _state;
    std::string _dimension;
  };

  void
  set(std::string const &dimension, uint64_t value);

  ValProxy
  at(std::string const &dimension);

  [[nodiscard]] uint64_t
  at(std::string const &dimension) const;

  ValProxy
  operator[](std::string const &dimension);

  uint64_t
  operator[](std::string const &dimension) const;

  State(State const &s);
  State(State &&s) noexcept;
  State &
  operator=(State const &s);
  State &
  operator=(State &&s) noexcept;

  explicit State(AnyStateImplSPtr pimpl);
};

//--------------------------------------------------------------------------------------

struct StateSpace {
  static StateSpaceCSPtr
  factory(std::map<std::string, uint64_t> const &dimensions);

  /**
   * @brief
   * @param values Map of dimension and value pairs. Default value for undefined dimensions are 0
   * @return
   */
  [[nodiscard]] virtual State
  state(std::map<std::string, uint64_t> values) const = 0;

  [[nodiscard]] State
  state() const {
    return state({});
  }

  [[nodiscard]] virtual SuperState
  super_state(std::map<std::string, std::set<uint64_t>> values) const = 0;

  [[nodiscard]] SuperState
  super_state() const {
    return super_state({});
  }

  [[nodiscard]] virtual SuperState
  empty_super_state() const = 0;

  [[nodiscard]] virtual SuperState
  power_super_state() const = 0;
};

}// namespace Ferguson::State

#endif//FERGUSON_INCLUDE_FERGUSON_STATE_STATE_H
