// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef INTERRUPTABLE_H_
#define INTERRUPTABLE_H_

/* Local */
#include <ferguson/synchronizers/synchronizer.h>

namespace Ferguson::Synchronizer{

// Exception thrown when the thread is interrupted
class thread_interrupted_error: public std::runtime_error {
public:
  thread_interrupted_error(): std::runtime_error("InterruptableThread interrupted") {}
};

/******************************************************************************/
/* InterruptableHost class */
/**
 * @brief Class to be used by thread owner to control
 * initialization and termination of thread.
 */
class InterruptableHost : public virtual Synchronizer {
  NOCOPYASSIGN(InterruptableHost);

public:
  InterruptableHost() = default;
  /*** METHODS ***/

  /** 
   * @brief Execute procedure in thread safe manner on host side
   * @param procedure Procedure to be executed
   * @param deadline The deadline for the operation.
   * @param fail_if_true Function to be checked for termination signal
   * @param delay_until_true Function to be checked for delay signal
   * @return \b true if procedure executed successfully, \b false if terminated
   */
  virtual bool
  hostSafeExecute(std::function<void()> const &procedure,//
                  TimePoint deadline,                    //
                  const NullaryBoolF &fail_if_true,      //
                  const NullaryBoolF &delay_until_true) = 0;

  /**
   * @brief Wait until conditions met then execute
   * @details This method takes max allowed delay into account.
   */
  virtual bool
  hostWaitUntil(TimePoint deadline,              //
                const NullaryBoolF &fail_if_true,//
                const NullaryBoolF &delay_until_true) = 0;

  /** 
   * @brief Request thread to initialize
   */
  virtual void
  requestInitialize() = 0;

  /** @brief Will return \b true if thread signals that it has initialized
   */
  virtual bool
  isInitialized() = 0;

  /**
   * @brief For host to wait indefinitely for initialization signal from thread. Blocking method.
   * @return \b true if initialized before return, \b false if returned before due to timeout.
   */
  bool
  waitInitialized() {
    return waitInitialized(TimeUnit::max());
  }
  /**
   * @brief For host to wait for initialization signal from thread. Blocking method.
   * @param timeout Max amount of time to wait.
   * @return \b true if initialized before return, \b false if returned before due to timeout.
   */
  virtual bool
  waitInitialized(TimeUnit timeout) = 0;

  /**
   * @brief Request thread to terminate
   */
  virtual void
  requestTerminate() = 0;

  /**
   * @brief Will return \b true if thread confirms that it is waiting for termination
   */
  virtual bool
  isWaitingTermination() = 0;

  /** 
   * @brief Will return \b true if thread confirms that it has been terminated
   */
  virtual bool
  isTerminated() = 0;

  /**
   * @brief For host to wait indefinitely for termination signal from thread. Blocking method.
   * @return \b true if terminated before return, \b false if returned before due to timeout.
   */
  bool
  waitTerminated() {
    return waitTerminated(TimeUnit::max());
  }

  /**
   * @brief For host to wait for termination signal from thread. Blocking method.
   * @param timeout Max amount of time to wait.
   * @return \b true if terminated before return, \b false if returned before due to timeout.
   */
  virtual bool
  waitTerminated(TimeUnit timeout) = 0;

  /**
   * @brief throw the exception propagated by thread
   */
  virtual std::exception_ptr
  threadException() = 0;

private:
  using Synchronizer::safeExecute;
  using Synchronizer::waitUntil;
};

DEFINE_SMARTPTR(InterruptableHost);

/******************************************************************************/
/* InterruptableThread class */

/**
 * @brief Class to be used by created thread to accommodate commands from the
 * owner
 */
class InterruptableThread: public virtual Synchronizer::Synchronizer {
  NOCOPYASSIGN(InterruptableThread);

public:
  InterruptableThread() = default;
  /*** METHODS ***/

  /** 
   * @brief Execute procedure in thread safe manner on host side
   * @param procedure: Procedure to be executed
   * @param deadline: The deadline for the operation.
   * @param fail_if_true: Function to be checked for termination signal
   * @param delay_until_true: Function to be checked for delay signal
   * @return \b true if procedure executed successfully, \b false if terminated
   */
  virtual bool
  threadSafeExecute(std::function<void()> const &procedure,//
                    TimePoint deadline,                    //
                    const NullaryBoolF &fail_if_true,      //
                    const NullaryBoolF &delay_until_true) = 0;

  /**
   * @brief Wait until conditions met then execute
   * @details This method takes max allowed delay into account.
   */
  virtual bool
  threadWaitUntil(TimePoint deadline,              //
                  const NullaryBoolF &fail_if_true,//
                  const NullaryBoolF &delay_until_true) = 0;

  /**
   * @brief
   * @return  Return \b true if host requested initialization
   */
  virtual bool
  isInitializeRequested() = 0;

  /**
   * @brief Signal host that initialization is completed
   */
  virtual void
  setInitialized() = 0;

  /**
   * @brief
   * @warning This method should be checked at least once in every \b response_time given to factory of this class
   * @return Return \b true if host requested termination
   */
  virtual bool
  isTerminateRequested() = 0;

  /**
   * @brief Signal host that termination is completed
   */
  virtual void
  setTerminated() noexcept = 0;

  /**
   * @brief For thread to wait for initialization signal from host. Blocking method.
   * @return \b true if initialized before return, \b false if returned before due to termination request.
   *         \sa Ferguson::Interruptable::InterruptableHost::requestTerminated
   */
  virtual bool
  waitInitializationRequest() = 0;

  /**
   * @brief Publish result data to host
   * @param deadline The deadline for the operation.
   */
  virtual void
  sleepUntil(TimePoint deadline) = 0;

  /**
   * @brief Publish result data to host
   * @param duration The duration to sleep.
   */
  virtual void
  sleepFor(TimeUnit const &duration) = 0;

  /**
   * @brief Publish the exception to the host
   * @param exception The exception to be published.
   */
  virtual void
  throwException(std::exception_ptr exception) = 0;

private:
  using Synchronizer::safeExecute;
  using Synchronizer::waitUntil;
};
DEFINE_SMARTPTR(InterruptableThread);

/** @brief Factory method to create object
 */
std::pair<InterruptableHostSPtr, InterruptableThreadSPtr>
interruptableFactory(TimeUnit response_time);

/**
 * @brief Design template for thread run method. It can be used directly with
 * or can be used as a base for proper thread method
 * @param sync Synchronization object
 * @param procedure Procedure to be executed
 */
[[maybe_unused]]
static void
interruptableRun(InterruptableThreadSPtr const &sync, std::function<void()> const &procedure) {
  if(!sync->waitInitializationRequest()) {
    sync->setTerminated();
    return;
  }
  sync->setInitialized();
  procedure();
  sync->setTerminated();
}

}// namespace Ferguson::Interruptable

#endif// INTERRUPTABLE_H_
