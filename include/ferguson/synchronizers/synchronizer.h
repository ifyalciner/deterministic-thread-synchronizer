// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef SYNCHRONIZER_H_
#define SYNCHRONIZER_H_

/* CPP */
#include <functional>
#include <memory>

/* Boost */
#include <boost/chrono.hpp>
#include <boost/thread/mutex.hpp>

/* Local */
#include <ferguson/time.h>
#include <ferguson/utils.h>

namespace Ferguson::Synchronizer {

/******************************************************************************/
class response_timeout_error: public std::runtime_error {
public:
  response_timeout_error(): runtime_error("File not found") {}

  explicit response_timeout_error(std::string const &msg): runtime_error(msg.c_str()) {}
};

/**
 * @brief Utility class for timed sensitive procedures and waiting
 */
class Synchronizer {
  NOCOPYASSIGN(Synchronizer);

public:
  /*** DEFINITIONS ***/
  using MutexType = boost::timed_mutex;
  using LockType  = boost::unique_lock<MutexType>;

  /*** METHODS ***/

  /**
   * @brief Wait until conditions met then execute
   * @details This method takes max allowed delay into account.
   *          Can be used to wait on a condition without providing a procedure
   *          Can be used to thread safely execute a procedure without a condition
   *
   * @warning Execution time of any of the input methods should NOT exceed
   * responseTime
   *
   * @param procedure: method to be executed.
   * @param deadline: Wait deadline for default lock to be available
   * @param fail_if_true: boolean method returns \b true if safeExecute should terminate early and return false
   * @param delay_until_true: While this method returns \b false safeExecute will wait before attempting to execute the procedure
   *
   * @attention \p delay_until_true and \p fail_if_true will be checked on \b max_terminate_delay
   *            provided to factory or when \a update() method is called.
   *
   * @attention \p delay_until_true has priority over fail_if_true
   *            if delay_until_true is \b true while fail_if_true is \b false method
   *            will return as expected.
   *
   * @attention \p max_terminate_delay refers to the argument provided to the factory of this class
   *
   * @return true if successfully executed
   * @return false if thread is terminated
   */
  virtual bool
  safeExecute(std::function<void()> const &procedure,//
              TimePoint deadline,                    //
              const NullaryBoolF &fail_if_true,      //
              const NullaryBoolF &delay_until_true) = 0;

  /**
   * @brief Simple wrapper around safeExecute with no procedure \sa safeExecute. Useful for solely waiting operations.
   */
  virtual bool
  waitUntil(TimePoint deadline,              //
            const NullaryBoolF &fail_if_true,//
            const NullaryBoolF &delay_until_true) = 0;

  /**
   *  @brief Force emitting update signal to all waiting threads
   *  @details: It will force checking of all wait conditions including `delay_until_true` and `fail_if_true`
   *     methods provided to `safeExecute` and `waitUntil` methods by waiting threads.
   */
  virtual void
  update() noexcept = 0;

  /**
   * @brief Create a Synchronizer object
   * @param max_terminate_delay: max response time
   */
  static std::shared_ptr<Synchronizer>
  factory(TimeUnit const &max_terminate_delay);

protected:
  Synchronizer() = default;
};

DEFINE_SMARTPTR(Synchronizer);

}// namespace Ferguson::Synchronizer

#endif// SYNCHRONIZER_H_
