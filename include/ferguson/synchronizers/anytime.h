// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef FERGUSON_INCLUDE_TREAD_ACTION_H
#define FERGUSON_INCLUDE_TREAD_ACTION_H

#include <any>
#include <optional>
#include <utility>

#include <ferguson/synchronizers/interruptable.h>
#include <ferguson/utils.h>

namespace Ferguson::Synchronizer {

class AnytimeHost: public virtual InterruptableHost {
  NOCOPYASSIGN(AnytimeHost);

public:
  AnytimeHost() = default;

  /**
   * @brief
   */
  virtual std::any
  getProduct(TimePoint deadline) = 0;

  /**
   * @brief
   */
  std::any
  getProduct() {
    return getProduct(TimePoint::max());
  }

  template<typename T>
  std::optional<T>
  getProduct(TimePoint deadline) {
    auto feedback = getProduct(deadline);
    if(feedback.has_value()) {
      return std::any_cast<T>(feedback);
    }
    else {
      return std::nullopt;
    }
  }

  template<typename T>
  std::optional<T>
  getProduct() {
    return getProduct<T>(TimePoint::max());
  }
};
DEFINE_SMARTPTR(AnytimeHost);

class AnytimeThread: public virtual InterruptableThread {
  NOCOPYASSIGN(AnytimeThread);

public:
  AnytimeThread() = default;

  /**
   * @brief Publish feedback data to host
   * @param feedback
   */
  virtual void
  publishProduct(std::any feedback) = 0;

  template<typename T>
  void
  publishProduct(T const &feedback) {
    publishProduct(std::any(feedback));
  }
};

DEFINE_SMARTPTR(AnytimeThread);

/**
 * @brief Factory method to produce \b function synchronizer
 * @param max_terminate_delay: response time
 * @param feedback_callback : Optional callback. If callback is provided feedback data is not queued.
 * @param result_callback : Optional callback.
 * @return Pair of AnytimeHost and AnytimeThread interface of synchronizer objects.
 */
std::pair<AnytimeHostSPtr, AnytimeThreadSPtr>
anytimeFactory(TimeUnit const &max_terminate_delay);

}// namespace Ferguson::Synchronizer

#endif//FERGUSON_INCLUDE_TREAD_ACTION_H
