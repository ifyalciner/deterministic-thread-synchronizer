// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

/* Local */
#include <ferguson/synchronizers/interruptable.h>


namespace Ferguson::Communicator {

/******************************************************************************/
/*** DEFINITIONS ***/

struct MessageBase {
  TimePoint time;

  void
  reset_time(TimePoint const &new_time) {
    time = new_time;
  }
};

struct InputBase: public MessageBase {
  virtual ~InputBase()= default;
};

DEFINE_SMARTPTR(InputBase);

struct OutputBase: public MessageBase {
  virtual ~OutputBase()= default;
};
DEFINE_SMARTPTR(OutputBase);

using ReturnCallbackType = std::function<void(OutputBaseCSPtr)>;

/******************************************************************************/
/* Host class */

class Host: public virtual Interruptable::Host {
  NOCOPYASSIGN(Host);

public:
  Host() = default;
  /*** METHODS ***/
  /**
   *
   * @param data
   */
  virtual void
  toThread(InputBaseCSPtr data) = 0;

  // Copying not allowed
};

DEFINE_SMARTPTR(Host);

/******************************************************************************/
/* Thread class */

class Thread: public virtual Interruptable::Thread {
  NOCOPYASSIGN(Thread);

public:
  Thread() = default;

  /**
   *
   * @return
   */
  virtual std::pair<bool, InputBaseCSPtr>
  fromHost() = 0;
  /**
   *
   * @return
   */
  virtual bool
  isDataAvailable() = 0;
  /**
   *
   * @param output
   */
  virtual void
  toHost(OutputBaseCSPtr output) = 0;
};
DEFINE_SMARTPTR(Thread);

std::pair<HostSPtr, ThreadSPtr>
factory(
    TimeUnit response_time             = TimeUnit(TimeUnit::max()),//
    const ReturnCallbackType& return_callback = [](const OutputBaseCSPtr&) {});

/**
 * @brief Design template for thread run method. It can be used directly with
 * or can be used as a base for proper thread method
 * @param sync Synchronization object
 * @param procedure Procedure to be executed
 */
[[maybe_unused]] static void
run(ThreadSPtr sync,//
    std::function<OutputBaseCSPtr(InputBaseCSPtr)> procedure) {
  auto parent_procedure = [&]() {
    InputBaseCSPtr data;
    bool received;
    while(true) {
      std::tie(received, data) = sync->fromHost();
      if(received) {
        auto output = procedure(data);
        if(sync->isTerminateRequested()) {
          return;
        }
        sync->toHost(output);
      }
      if(sync->isTerminateRequested()) {
        return;
      }
    }
  };

  Interruptable::run(sync, parent_procedure);
}

} // namespace Ferguson::Communicator


#endif// COMMUNICATOR_H_
