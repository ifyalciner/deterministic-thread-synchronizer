// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FERGUSON_INCLUDE_FERGUSON_TIME_H
#define FERGUSON_INCLUDE_FERGUSON_TIME_H

/* Boost */
#include <boost/chrono.hpp>
#include <boost/thread.hpp>

namespace Ferguson {
/* Currently Ferguson library uses milliseconds as main time unit */

#ifdef DEBUGGING
/**
 * @brief During debugging chrono clocks don't stop while `Clock` class
 *          is based on thread updates and stops with the rest of the system.
 */
struct Clock {
  typedef boost::chrono::milliseconds duration;
  typedef duration::rep rep;
  typedef duration::period period;
  typedef boost::chrono::time_point<Clock> time_point;
  static const bool is_steady = false;

  static time_point
  now() noexcept;
};
#else
using Clock = boost::chrono::steady_clock;
#endif

using TimePoint = boost::chrono::time_point<Clock>;
using TimeUnit  = boost::chrono::milliseconds;

/**
 * @brief Convert a delay into a deadline with optional starting time point
 * @param timeout Delay
 * @param start From the point in time
 * @return deadline TimePoint from given start with delay
 */
TimePoint
toDeadline(TimeUnit const &timeout = TimeUnit::max(), TimePoint start = TimePoint());

#ifdef DEBUGGING
inline void
sleepUntil(TimePoint const &deadline) {
  // implement sleep until which uses Clock::now() and deadline
  while(Clock::now() < deadline) {
    boost::this_thread::sleep_for(TimeUnit(1));
  }
}

inline void
sleepFor(TimeUnit const &timeout) {
  // implement sleep for which uses Clock::now() and Clock::now() + timeout
  sleepUntil(toDeadline(timeout));
}

#else
inline void
sleepUntil(TimePoint const &deadline) {
  boost::this_thread::sleep_until(deadline);
}

inline void
sleepFor(TimeUnit const &timeout) {
  boost::this_thread::sleep_for(timeout);
}
#endif

}// namespace Ferguson

#endif//FERGUSON_INCLUDE_FERGUSON_TIME_H
