// MIT License
//
// Copyright (c) 2024 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/* Ferguson */
#include <ferguson/synchronizers/anytime.h>
#include <ferguson/threads/task_traits.h>
#include <ferguson/utils.h>

#include <any>
#include <functional>

namespace Ferguson::Threads {

class TaskBase {
public:
  explicit TaskBase(std::string class_name): _class_name(std::move(class_name)) {}
  virtual ~TaskBase() = default;

  virtual void
  run(std::function<void(std::any const &)> const &) = 0;

  [[nodiscard]] std::string const &
  className() const {
    return _class_name;
  }

private:
  std::string const _class_name;
};

DEFINE_SMARTPTR(TaskBase)

template<typename Product>
class Task: public TaskBase {
public:
  using ProductType = Product;

  Task(std::string const &class_name,//
       std::function<void(std::function<void(ProductType)>)> function)
      : TaskBase(class_name),//
        _function(std::move(function)) {}

  virtual ~Task() = default;

  void
  run(std::function<void(std::any const &)> const &product_callback) final {
    auto convert_ProductType = [product_callback = product_callback](ProductType product) {
      product_callback(product);// implicitly converts to std::any
    };
    _function(convert_ProductType);// implicitly converts the result to any
  }

private:
  std::function<void(std::function<void(ProductType)>)> const _function;
};

// Enable TaskObject if T has operator() and operator has signature defined in task_function_traits
template<typename T,                                                                                         //
         typename                       = std::enable_if_t<has_call_operator<T>::value>,                     //
         typename CallOperatorSignature = remove_class_t<decltype(&T::operator())>,                          //
         typename                       = std::enable_if<task_function_traits<CallOperatorSignature>::value>,//
         typename ProductType           = typename task_function_traits<CallOperatorSignature>::ProductType>
class TaskObject: public Task<ProductType> {

public:
  /**
   * @brief Construct a TaskObject with a given object.
   * @details The object should have a call operator with the signature defined in task_function_traits.
   * @tparam Args Arguments to be passed to the object's constructor.
   * @param args Arguments to be passed to the object's constructor.
   * @return TaskObject
   * @warning All arguments passed for the constructor should be copyable.
   * */
  template<typename... Args>
  explicit TaskObject(Args &&...args)
      : Task<ProductType>(std::string(typeid(T).name()),//
                          [=](std::function<void(ProductType)> const &product) {
                            T obj(args...);
                            return obj(product);
                          }) {}
};

template<typename T,//
         typename... Args>
std::shared_ptr<TaskObject<T>>
make_shared_task_object(Args... args) {
  return std::make_shared<TaskObject<T>>(std::forward<Args>(args)...);
}

template<typename T,                                                                                         //
         typename                       = std::enable_if_t<has_call_operator<T>::value>,                     //
         typename CallOperatorSignature = remove_class_t<decltype(&T::operator())>,                          //
         typename                       = std::enable_if<task_function_traits<CallOperatorSignature>::value>,//
         typename ProductType           = typename task_function_traits<CallOperatorSignature>::ProductType>
class TaskFunction: public Task<ProductType> {
public:
  // create a constructor which accepts a lambda function
  explicit TaskFunction(T function): Task<ProductType>(std::string(typeid(T).name()), std::move(function)) {}
};

template<typename T>
std::shared_ptr<TaskFunction<T>>
make_shared_task_function(T &&function) {
  return std::make_shared<TaskFunction<T>>(std::forward<T>(function));
}

}// namespace Ferguson::Threads
