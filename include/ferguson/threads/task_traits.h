// MIT License
//
// Copyright (c) 2024 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//
// Created by f on 06/06/24.
//

#ifndef FERGUSON_TASK_TRAITS_H
#define FERGUSON_TASK_TRAITS_H

#include <functional>
#include <type_traits>

// Helper to detect the existence of operator()
template<typename T, typename = void>
struct has_call_operator: std::false_type {};

template<typename T>
struct has_call_operator<T, std::void_t<decltype(&T::operator())>>: std::true_type {};

//
template<typename T>
struct product_callback_traits;

template<typename F>
struct product_callback_traits<std::function<void(F)>> {
  using ProductType = F;
};

template<typename F>
using product_callback_traits_t = typename product_callback_traits<F>::ProductType;

// Remove class pointer
template<typename T>
struct remove_class {
  using type = T;
};

template<typename C, typename R, typename... A>
struct remove_class<R (C::*)(A...)> {
  using type = R(A...);
};

template<typename C, typename R, typename... A>
struct remove_class<R (C::*)(A...) const> {
  using type = R(A...);
};

template<typename T>
using remove_class_t = typename remove_class<T>::type;

template<typename T>
struct lambda_traits: public lambda_traits<decltype(&T::operator())> {};

template<typename ClassType, typename ReturnType, typename... Args>
struct lambda_traits<ReturnType (ClassType::*)(Args...) const> {
  using signature = ReturnType(Args...);
};

// Return the same type if it is not lambda

template<typename T>
using lambda_signature_t = typename lambda_traits<T>::signature;

// Remove std::function
template<typename T>
struct remove_std_function {
  using type = T;
};

template<typename R, typename... A>
struct remove_std_function<std::function<R(A...)>> {
  using type = R(A...);
};

template<typename T>
using remove_std_function_t = typename remove_std_function<T>::type;

// Shortcut for remove reference and cv
template<typename T>
using remove_cvref_t = std::remove_cv_t<std::remove_reference_t<T>>;

// TaskBase function traits
template<typename T>
struct task_function_traits: std::false_type {};

template<typename TProductCallbackType>
struct task_function_traits<void(TProductCallbackType)>: std::true_type {
  using ProductType         = product_callback_traits_t<remove_cvref_t<TProductCallbackType>>;
  using ProductCallbackType = TProductCallbackType;
};

// Identity
// This is a helper to get the type of the first argument
template<typename T>
struct identity {
  using type = T;
};

template<typename T>
using identity_t = typename identity<T>::type;

#endif//FERGUSON_TASK_TRAITS_H
