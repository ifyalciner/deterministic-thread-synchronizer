from setuptools import setup, find_packages
from setuptools.command.install import install
import subprocess
import os

current_dir = os.path.dirname(os.path.realpath(__file__))
script_dir = os.path.join(current_dir, "prep.sh")
resources_dir = os.path.join(current_dir, "resources", "resources.qrc")
resources_py_dir = os.path.join(current_dir, "companion", "autogen", "resources.py")

class Install(install):
    def run(self):
        print("Running prep.sh")
        try:
            autogen_dir = os.path.join(current_dir, "companion", "autogen")
            os.mkdir(autogen_dir)
            open(os.path.join(autogen_dir, "__init__.py"), "w").close()
        except FileExistsError:
            pass

        subprocess.call(f"pyrcc5 {resources_dir} -o {resources_py_dir}".split())
        with open(resources_py_dir, "r") as f:
            file_data= f.read()
        file_data = file_data.replace("PyQt5", "PySide2")
        with open(resources_py_dir, "w") as f:
            f.write(file_data)
        install.run(self)

setup(
   name='fcompanion',
   packages=find_packages(),
   entry_points={ 'gui_scripts': [ 'fcompanion = companion.desktop.main:main' ]},
   cmdclass= {'install': Install},
   data_files=[("companion/autogen", ['companion/autogen/resources.py'])]
)
