#### Log Message Format

* time
* from
* type: Commands
* content

#### Commands types

##### Message

* \_t: text message

##### Action

* \_ac\_c #id #parent\_id #name #type: Action created
* \_ac\_s: Action started
* \_ac\_r: Action completed (resulted)
* \_ac\_f: Action failed
* \_ac\_p: Action preempted by parent
* \_ac\_t: Action terminated by action

##### Policy

* \_pl\_e: Policy Executing
* \_pl\_f: Policy Failed

##### State

* \_s\_c: State Changed

##### Feedback

* \_fb\_r #from: Feedback received
* \_fb\_s: Feedback sent

