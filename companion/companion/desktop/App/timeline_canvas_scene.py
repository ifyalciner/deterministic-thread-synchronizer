#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtWidgets as Qw

from companion.core import tree
from . import action
from . import action_instance
from . import feedback_graphics
from . import graphics_data
from . import log_graphics
from . import time_stamp


class TimelineCanvasScene(Qw.QGraphicsScene):
    def __init__(self,
                 atm: tree.ActionManager,
                 height: int,
                 orig_time_resolution: float,
                 time_gauge_height: int,
                 parent):
        super().__init__(parent)

        self.gd: typing.Final[graphics_data.GraphicsData] = graphics_data.GraphicsData(atm, dict())

        self.height: typing.Final[int] = height
        self.time_gauge_height: typing.Final[int] = time_gauge_height

        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self._time_resolution: float = orig_time_resolution

        self._no_actions = None
        self._end_time = None
        self._last_time_stamp_added: float = None
        self._visible_actions: typing.List[str] = []
        self._visible_timeframe: typing.Tuple[float, float] = (0, float("inf"))
        self._margin: typing.Final[int] = 20
        self._time_items: typing.List[time_stamp.TimeStampGraphicsItem] = []
        self.update_scene_rect()

    def change_action_visibility(self, visible_actions: typing.List[str], time_frame: typing.Tuple[float, float]):
        self._visible_actions = visible_actions.copy()
        self._visible_timeframe = tuple(time_frame)
        ago_dict = self.gd.action_graphic_objects.copy()
        for aid, ago in ago_dict.items():
            ago.setVisible(False)
        for r, a in enumerate(self._visible_actions):
            ago = self.gd.action_graphic_objects[a]
            ago.setPos((ago.action.created_at - self.gd.atm.init_time) * self._time_resolution,
                       self.time_gauge_height + (r * self.height) + (self._margin / 2))
            ago.setVisible(True)
        self.update_scene_rect()

    @Qc.Slot(float, float)
    def set_viewing_timeframe(self, frm: float, to: float):
        self.change_action_visibility(self._visible_actions, (frm, to))

    @Qc.Slot(list)
    def set_visible_actions(self, visible_actions: typing.List[str]):
        self.change_action_visibility(visible_actions, self._visible_timeframe)

    @Qc.Slot(float)
    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        for _, v in self.gd.action_graphic_objects.items():
            v.update_time_resolution(self._time_resolution)
        for ts in self._time_items:
            ts.update_time_resolution(self._time_resolution)

        self.update_scene_rect()
        # views: typing.List[Qw.QGraphicsView] = self.views()
        # if len(views) > 0:
        #    view: Qw.QGraphicsView = views[0]
        #    self.update(view.viewport().rect())

    @Qc.Slot(float, float)
    def update_timeframe(self, end: float):

        if self._last_time_stamp_added is None:
            self._last_time_stamp_added = float(round(((self.gd.atm.init_time - 0.1) // 5) * 5))

        while end - self._last_time_stamp_added > 5.0:
            self._last_time_stamp_added = self._last_time_stamp_added + 5.0

            self._time_items.append(time_stamp.TimeStampGraphicsItem(time_stamp_sec=self._last_time_stamp_added,
                                                                     start_time_stamp=self.gd.atm.init_time,
                                                                     orig_time_resolution=self.orig_time_resolution,
                                                                     parent=None))
            self.addItem(self._time_items[-1])
            self._time_items[-1].update_time_resolution(self._time_resolution)
        self._end_time = end
        for _, v in self.gd.action_graphic_objects.items():
            v.update_timeframe(self._end_time)
        self.update_scene_rect()

    def update_scene_rect(self):
        if not self.gd.atm.init_time or not self._end_time:
            return
        prev_rect = self.sceneRect()
        self.setSceneRect(0, 0, (self._end_time - self.gd.atm.init_time) * self._time_resolution,
                          len(self._visible_actions) * self.height)
        self.update(prev_rect)
        self.update(self.sceneRect())
        views: typing.List[Qw.QGraphicsView] = self.views()
        if len(views) > 0:
            view: Qw.QGraphicsView = views[0]
            self.update(view.viewport().rect())

    @Qc.Slot(str)
    def action_add(self, action_name: str):
        act = self.gd.atm.action(action_name)
        orig_pos = Qc.QPointF((act.created_at - self.gd.atm.init_time) * self.orig_time_resolution, 0)
        ago = self.gd.action_graphic_objects[action_name] = action.ActionGO(gd=self.gd,
                                                                            action=act,
                                                                            height=self.height - self._margin,
                                                                            orig_time_resolution=self.orig_time_resolution,
                                                                            orig_pos=orig_pos,
                                                                            parent=None)
        act.add_new_instance_post_callbacks(self.instance_add)
        act.add_new_child_post_callbacks(lambda x: ago.action_window.update() if ago.action_window else None)
        self.addItem(ago)
        ago.update_time_resolution(self._time_resolution)
        # ago.setEnabled(True)
        ago.setVisible(False)
        # self._action_window[action_name] = ActionWindow(self.gd.atm, action_name, self)
        self.update_scene_rect()

        # if parent action object of act has an action_window exist, then update it
        if act.parent_name:
            parent = self.gd.atm.action(act.parent_name)
            if parent:
                parent.add_child(act)

    def instance_counter_add(self, instance_id: int, timestamp: float):
        instance = self.gd.atm.instance(instance_id)
        action_go = self.gd.action_graphic_objects[instance.name]
        instance_go = action_go.instance_go[instance_id]
        orig_pos = Qc.QPointF((timestamp - instance.created_at) * self.orig_time_resolution,
                              float(instance_go.height) / 2 - feedback_graphics.FeedbackCounterGO.radius / 2)

        ai_counter_go = action_instance.ActionInstanceCounterGO(orig_pos=orig_pos,
                                                                orig_time_resolution=self.orig_time_resolution,
                                                                parent=instance_go)
        instance_go.ai_counter.append(ai_counter_go)

        ai_counter_go.update_time_resolution(self._time_resolution)
        ai_counter_go.update()
        instance_go.update(instance_go.rect())
        return ai_counter_go

    def instance_add(self, instance_id: int):
        instance = self.gd.atm.instance(instance_id)
        act = self.gd.atm.action(instance.name)
        action_go = self.gd.action_graphic_objects[instance.name]
        orig_pos = Qc.QPointF((instance.created_at - act.created_at) * self.orig_time_resolution,
                              action_go.margin / 2)

        aic_go = None
        if act.parent_name != '':
            aic_go = self.instance_counter_add(instance_id=instance.parent_id,
                                               timestamp=instance.created_at)
        instance_go = action_instance.ActionInstanceGO(gd=self.gd,
                                                       instance=instance,
                                                       height=action_go.height - action_go.margin,
                                                       orig_time_resolution=action_go.orig_time_resolution,
                                                       orig_pos=orig_pos,
                                                       aic_go=aic_go,
                                                       parent=action_go)
        action_go.instance_go[instance_id] = instance_go

        instance.add_log_post_callbacks(self.log_add)
        instance.add_feedback_post_callbacks(self.feedback_add)

        instance_go._last_update = instance.created_at
        instance_go.update_time_resolution(self._time_resolution)
        if action_go.action_window:
            action_go.action_window.update()

    def log_add(self, instance_id: int, log_id: int):
        instance = self.gd.atm.instance(instance_id)
        action_go = self.gd.action_graphic_objects[instance.name]
        instance_go = action_go.instance_go[instance_id]
        log = instance.text_logs[log_id]
        orig_pos = Qc.QPointF((log[0] - instance.created_at) * self.orig_time_resolution,
                              float(instance_go.height) / 2 - log_graphics.LogGO.radius / 2)
        instance_go.logs.append(
            log_graphics.LogGO(orig_pos=orig_pos,
                               orig_time_resolution=self.orig_time_resolution,
                               content=log[1],
                               parent=instance_go))
        log_go = instance_go.logs[-1]
        log_go.update_time_resolution(self._time_resolution)
        instance_go.update(instance_go.rect())
        if instance_go.instance_window:
            instance_go.instance_window.update()

    def feedback_counter_add(self, fb_go: feedback_graphics.FeedbackGO, instance_id: int, timestamp: float):
        instance = self.gd.atm.instance(instance_id)
        action_go = self.gd.action_graphic_objects[instance.name]
        instance_go = action_go.instance_go[instance_id]
        orig_pos = Qc.QPointF((timestamp - instance.created_at) * self.orig_time_resolution,
                              float(instance_go.height) / 2 - feedback_graphics.FeedbackCounterGO.radius / 2)

        fb_counter_go = feedback_graphics.FeedbackCounterGO(orig_pos=orig_pos,
                                                            orig_time_resolution=self.orig_time_resolution,
                                                            fb_go=fb_go, parent=instance_go)
        instance_go.ai_counter.append(fb_counter_go)

        fb_counter_go.update_time_resolution(self._time_resolution)
        fb_counter_go.update()
        instance_go.update(instance_go.rect())

    def feedback_add(self, instance_id: int, fb_id: int):
        instance = self.gd.atm.instance(instance_id)
        action_go = self.gd.action_graphic_objects[instance.name]
        instance_go = action_go.instance_go[instance_id]
        fb = instance.feedbacks[fb_id]
        orig_pos = Qc.QPointF((fb[0] - instance.created_at) * self.orig_time_resolution,
                              float(instance_go.height) / 2 - feedback_graphics.FeedbackGO.radius / 2)
        fb_go = feedback_graphics.FeedbackGO(orig_pos=orig_pos, orig_time_resolution=self.orig_time_resolution,
                                             content=str(fb[1]),
                                             parent=instance_go)
        instance_go.feedbacks.append(fb_go)
        fb_go.update_time_resolution(self._time_resolution)
        instance_go.update(instance_go.rect())
        self.feedback_counter_add(fb_go=fb_go, instance_id=int(fb[1]['from']), timestamp=fb[0])
        if instance_go.instance_window:
            instance_go.instance_window.update()
