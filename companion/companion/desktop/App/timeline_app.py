#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import json
import struct
import time

# 3rd Party
import PySide2.QtCore as Qc
import PySide2.QtNetwork as Qn
import PySide2.QtWidgets as Qw

# Project
from companion.core import ActionManager
from .timeline_canvas_view import TimelineCanvasScene, TimelineCanvasView
from .timeline_tree import TimelineTree


class FergusonContact(Qc.QObject):
    finished = Qc.Signal()

    def __init__(self, ip: str, port: int, action_tree_manager: ActionManager):
        super().__init__()
        self._terminating = False
        self._ip = ip
        self._port = port
        self._action_tree_manager = action_tree_manager
        self._psocket = Qn.QTcpSocket()
        self._psocket.readyRead.connect(self.read_data)
        self._psocket.connectToHost(self._ip, self._port)

    def recv_msg(self):
        # Read message length and unpack it into an integer
        raw_msglen = self.recvall(4)
        if not raw_msglen:
            return None
        msglen = struct.unpack('>I', raw_msglen)[0]
        # Read the message data
        return self.recvall(msglen)

    def recvall(self, n: int):
        # Helper function to recv n bytes or return None if EOF is hit
        data = bytearray()
        while len(data) < n:
            packet = self._psocket.read(n - len(data))
            if not packet:
                return None
            data.extend(packet)
        return data

    @Qc.Slot()
    def read_data(self):
        msg = self.recv_msg()
        if msg:
            self._action_tree_manager.process_logs(json.loads(msg))


class TimelineApp(Qw.QWidget):
    sig_time_progress = Qc.Signal(float)
    sig_action_added = Qc.Signal(str)

    def __init__(self, name: str, atm: ActionManager, parent=None):
        super().__init__(parent=parent)
        self.name = name
        self.atm = atm

        self._start_time = None
        self._current_time = self.atm.init_time
        self._time_updater = Qc.QTimer(self)
        self._time_updater.timeout.connect(self.publish_time)
        self._time_updater.setInterval(50)
        self._time_updater.start()

        self._splitter_layout = Qw.QVBoxLayout(self)
        self._splitter = Qw.QSplitter(self)
        self._splitter_layout.addWidget(self._splitter)
        self._splitter.setSizePolicy(Qw.QSizePolicy(Qw.QSizePolicy.MinimumExpanding,
                                                    Qw.QSizePolicy.MinimumExpanding))
        self._splitter.setOrientation(Qc.Qt.Horizontal)
        self._splitter.setObjectName("splitter")
        self._splitter.setContentsMargins(0, 0, 0, 0)

        # Setup Left Side
        self._leftVerticalLayoutWidget = Qw.QWidget(self._splitter)
        self._splitter.setCollapsible(self._splitter.indexOf(self._leftVerticalLayoutWidget), False)
        self._leftVerticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self._leftVerticalLayoutWidget.setContentsMargins(0, 0, 0, 0)
        self._leftVerticalLayoutWidget.setSizePolicy(Qw.QSizePolicy.MinimumExpanding,
                                                     Qw.QSizePolicy.Minimum)
        self._leftVerticalLayout = Qw.QVBoxLayout(self._leftVerticalLayoutWidget)
        self._leftVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self._leftVerticalLayout.setObjectName("verticalLayout")
        self._leftVerticalLayout.setSpacing(0)

        # Setup Right Side
        self._rightVerticalLayoutWidget = Qw.QWidget(self._splitter)
        self._splitter.setCollapsible(self._splitter.indexOf(self._rightVerticalLayoutWidget), False)
        self._rightVerticalLayoutWidget.setObjectName("SplitterRightLayout")
        self._rightVerticalLayoutWidget.setContentsMargins(0, 0, 0, 0)
        sp = Qw.QSizePolicy(Qw.QSizePolicy.Expanding,
                            Qw.QSizePolicy.Minimum)
        sp.setHorizontalStretch(255)
        self._rightVerticalLayoutWidget.setSizePolicy(sp)
        self._rightVerticalLayout = Qw.QVBoxLayout(self._rightVerticalLayoutWidget)
        self._rightVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self._rightVerticalLayout.setObjectName("RightVerticalLayout")
        self._rightVerticalLayout.setAlignment(Qc.Qt.AlignTop | Qc.Qt.AlignLeft)

        height = 40
        init_time_resolution = 50
        time_gauge_height = 30

        ## Right: Setup Timeline Canvas
        self._timelineCanvasGraphicsScene = TimelineCanvasScene(atm=self.atm,
                                                                height=height,
                                                                orig_time_resolution=init_time_resolution,
                                                                time_gauge_height=time_gauge_height,
                                                                parent=self)
        self._timelineCanvasGraphicsView = TimelineCanvasView(atm=self.atm,
                                                              height=height,
                                                              orig_time_resolution=init_time_resolution,
                                                              time_gauge_height=time_gauge_height,
                                                              scene=self._timelineCanvasGraphicsScene,
                                                              parent=self._rightVerticalLayoutWidget)
        self._timelineCanvasGraphicsView.setSizePolicy(Qw.QSizePolicy.Expanding, Qw.QSizePolicy.Expanding)
        self._timelineCanvasGraphicsView.setMinimumSize(Qc.QSize(500, 500))
        self._timelineCanvasGraphicsView.setObjectName("graphicsView")
        self._rightVerticalLayout.addWidget(self._timelineCanvasGraphicsView)
        # self._timelineCanvasGraphicsView.setStyleSheet("background-color:blue;")

        ## Left: Setup Spacer
        spacerItem = Qw.QSpacerItem(0, time_gauge_height, Qw.QSizePolicy.Minimum, Qw.QSizePolicy.Fixed)
        self._leftVerticalLayout.addItem(spacerItem)

        ## Left:Setup Tree
        self._treeWidget = TimelineTree(self.atm, self._leftVerticalLayoutWidget)
        self._leftVerticalLayout.addWidget(self._treeWidget)

        # for testing
        # bk = Qg.QPalette()
        # bk.setColor(Qg.QPalette.Window, Qc.Qt.black)
        # self._treeWidget.setAutoFillBackground(True)
        # self._treeWidget.setPalette(bk)

        self._treeWidget.row_height = height

        # Signals
        self._timelineCanvasGraphicsView.sig_zoomed.connect(self._timelineCanvasGraphicsScene.update_time_resolution)
        self.atm.add_action_added_post_cb(self._timelineCanvasGraphicsScene.action_add)
        self.atm.add_action_added_post_cb(self._treeWidget.action_add_cb)
        self._treeWidget.verticalScrollBar().valueChanged.connect(self._timelineCanvasGraphicsView.vertical_slider_move)
        self._treeWidget.verticalScrollBar().sliderMoved.connect(self._timelineCanvasGraphicsView.vertical_slider_move)
        # self.atm.add_time_progress_post_cb(self.notify_time_progress)
        self.sig_time_progress.connect(self._timelineCanvasGraphicsScene.update_timeframe)
        self._timelineCanvasGraphicsView.sig_viewing_timeframe_changed.connect(
            self._treeWidget.set_viewing_timeframe)
        self._treeWidget.sig_visible_actions_changed.connect(self._timelineCanvasGraphicsView.set_visible_actions)
        self._treeWidget.sig_visible_actions_changed.connect(self._timelineCanvasGraphicsScene.set_visible_actions)

    def publish_time(self):
        if not self._start_time and self.atm.init_time:
            self._start_time = self.atm.init_time
            now = time.time()
            if self.atm.init_time > now:
                print(f"{self.atm.init_time} - {now}")

        # time_str = str(time.strftime("%H:%M:%S", time.localtime(self.atm)))
        # print(f"{time_str}")
        if self._start_time:
            self.sig_time_progress.emit(time.time())

    def notify_time_progress(self, atm: ActionManager):
        self.sig_time_progress.emit(atm.init_time, atm.cur_time)
