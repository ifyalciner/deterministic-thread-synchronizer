#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import time
import typing

import PySide2.QtCore as Qc
import PySide2.QtWidgets as Qw


class TimeStampGraphicsItem(Qw.QGraphicsTextItem):

    def __init__(self, time_stamp_sec: float, start_time_stamp: float, orig_time_resolution: float, parent):
        time_str = str(time.strftime("%H:%M:%S", time.localtime(time_stamp_sec)))
        super().__init__(time_str, parent)
        self._offset: typing.Final[float] = self.boundingRect().width() / 2
        self.orig_pos: typing.Final[Qc.QPointF] = Qc.QPointF(
            ((time_stamp_sec - start_time_stamp) * orig_time_resolution) - self._offset, 0)
        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)

    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        self.setPos(
            (((self.orig_pos.x() + self._offset) / self.orig_time_resolution) * self._time_resolution) - self._offset,
            self.pos().y())
        self.update()
