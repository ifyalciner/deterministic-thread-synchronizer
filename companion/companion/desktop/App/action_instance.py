#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtGui as Qg
import PySide2.QtWidgets as Qw

from companion.core.tree import ActionInstance
from . import feedback_graphics
from . import graphics_data
from . import instance_window
from . import log_graphics


class ActionInstanceCounterGOSigEmitter(Qc.QObject):
    sig_pos_updated = Qc.Signal()

    def __init__(self, parent):
        super().__init__()


class ActionInstanceCounterGO(Qw.QGraphicsEllipseItem):
    radius = 0.2

    # Rectangle is relative to current objects pos
    # https://stackoverflow.com/questions/9002094/qgraphicsitem-returns-wrong-position-in-scene-qt4-7-3
    def __init__(self, orig_pos: Qc.QPointF, orig_time_resolution: float, parent):
        super(ActionInstanceCounterGO, self).__init__(0, 0, 0, 0,
                                                      parent=parent)
        self.sig_emitter = ActionInstanceCounterGOSigEmitter(self)
        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.orig_pos: typing.Final[Qc.QPointF] = orig_pos
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)
        self.setFlag(Qw.QGraphicsItem.GraphicsItemFlag.ItemSendsScenePositionChanges)

        self.instance_window: typing.Optional[instance_window.InstanceWindow] = None

    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        self.setPos((self.orig_pos.x() / self.orig_time_resolution) * self._time_resolution, self.pos().y())

    def itemChange(self, change: Qw.QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        # self.sig_updated.emit()
        ret = super().itemChange(change, value)
        if change == Qw.QGraphicsItem.GraphicsItemChange.ItemScenePositionHasChanged:
            self.sig_emitter.sig_pos_updated.emit()
        return ret


class ActionInstanceGOSigEmitter(Qc.QObject):
    sig_open_instance_window = Qc.Signal(int)

    def __init__(self, parent):
        super().__init__()


class ActionInstanceGO(Qw.QGraphicsRectItem):
    def __init__(self,
                 gd: graphics_data.GraphicsData,
                 instance: ActionInstance,
                 height: int,
                 orig_time_resolution: float,
                 orig_pos: Qc.QPointF,
                 aic_go: typing.Optional[ActionInstanceCounterGO],
                 parent):
        super().__init__(0.0, 0.0, 0.0, float(height), parent=parent)
        self.sig_emitter = ActionInstanceGOSigEmitter(self)

        self.gd = gd
        self.aic_go = aic_go
        self.line: Qw.QGraphicsLineItem = None
        self.setZValue(2)
        self.instance: typing.Final[ActionInstance] = instance
        self.height: typing.Final[int] = height
        self.setFlag(Qw.QGraphicsItem.GraphicsItemFlag.ItemSendsScenePositionChanges)

        self.instance.add_destroyed_post_callbacks(self._destroyed)

        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.orig_pos: typing.Final[Qc.QPointF] = orig_pos
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)

        self._text = Qw.QGraphicsTextItem(str(instance.id), parent=self)
        self._text.setPos(0, 0)
        self._text.setZValue(1)

        self.logs: typing.List[log_graphics.LogGO] = []
        self.feedbacks: typing.List[FeedbackGO] = []
        self.feedback_counter: typing.List[feedback_graphics.FeedbackCounterGO] = []
        self.ai_counter: typing.List[ActionInstanceCounterGO] = []

        self.instance_window: typing.Optional[instance_window.InstanceWindow] = None

        if self.aic_go:
            self.aic_go.sig_emitter.sig_pos_updated.connect(self.update_arrow)

        for tm, log in self.instance.text_logs:
            self._process_new_log(tm, log)

        for tm, fb in self.instance.feedbacks:
            self._process_new_feedback(tm, fb)

        self._last_update = instance.created_at

        self._bk_color = Qg.QColor(Qc.Qt.cyan)
        self.update(self.rect())

    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        self.setPos((self.orig_pos.x() / self.orig_time_resolution) * self._time_resolution, self.pos().y())
        self.update_timeframe(self._last_update)
        for x in self.logs:
            x.update_time_resolution(self._time_resolution)
        for x in self.feedbacks:
            x.update_time_resolution(self._time_resolution)
        if self.aic_go:
            self.aic_go.update_time_resolution(time_resolution)
            self.update_arrow()
        self.update(self.rect())

    def _destroyed(self):
        self.update_timeframe(self._last_update)
        self.update(self.rect())

    def update_timeframe(self, end: float):
        if True or self.instance.alive(self._last_update):
            self._last_update = end
            width = (min(self.instance.destroyed_at, end) - self.instance.created_at) * self._time_resolution
            width = max(width, 0)
            self.prepareGeometryChange()
            self.setRect(0, 0, width, self.height)

    def open_window(self):
        if self.instance_window is None:
            self.instance_window = instance_window.InstanceWindow(self.gd, self.instance)
            self.instance_window.sig_closing.connect(self.closing_window)
            self.instance_window.show()
        else:
            self.instance_window.setFocus()
            self.instance_window.activateWindow()
            self.instance_window.show()

    @Qc.Slot()
    def closing_window(self):
        self.instance_window = None

    def set_background_color(self, color: Qg.QColor):
        self._bk_color = color
        self.update()

    def paint(self, painter: Qg.QPainter, option: Qw.QStyleOptionGraphicsItem,
              widget: typing.Optional[Qw.QWidget] = ...) -> None:
        painter.fillRect(self.rect(), self._bk_color)

    def itemChange(self, change: Qw.QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        ret = super().itemChange(change, value)
        if change == Qw.QGraphicsItem.GraphicsItemChange.ItemScenePositionHasChanged:
            if self.aic_go:
                self.update_arrow()
        return ret

    def update_arrow(self):
        self.draw_line()

    def draw_line(self):
        to = self.aic_go.sceneBoundingRect().center()
        diff = to - self.scenePos()
        if self.line:
            self.line.setVisible(False)
        self.line = Qw.QGraphicsLineItem(diff.x(), 0, diff.x(), diff.y(), parent=self)
        pfine = Qg.QPen(Qg.QColor(200, 100, 150, 255), 2)
        self.line.setPen(pfine)
        self.line.setZValue(-1)
        self.line.setVisible(True)

    def mouseDoubleClickEvent(self, event: Qw.QGraphicsSceneMouseEvent) -> None:
        # self.sig_emitter.sig_open_instance_window.emit(self.instance.id)
        self.open_window()
