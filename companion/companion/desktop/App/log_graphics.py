#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtWidgets as Qw


class LogGO(Qw.QGraphicsEllipseItem):
    radius = 13.0

    # Rectangle is relative to current objects pos
    # https://stackoverflow.com/questions/9002094/qgraphicsitem-returns-wrong-position-in-scene-qt4-7-3
    def __init__(self, orig_pos: Qc.QPointF, orig_time_resolution: float, content: str, parent):
        super(LogGO, self).__init__(0, 0, self.radius, self.radius,
                                    parent=parent)
        self.setZValue(5)

        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.orig_pos: typing.Final[Qc.QPointF] = orig_pos
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)

        self.setToolTip(content)
        self.setBrush(Qc.Qt.red)

    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        self.setPos((self.orig_pos.x() / self.orig_time_resolution) * self._time_resolution, self.pos().y())
