#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import math
import typing

# 3rd Party
import PySide2.QtCore as Qc
import PySide2.QtGui as Qg
import PySide2.QtWidgets as Qw

from companion.core import ActionManager
from .timeline_canvas_scene import TimelineCanvasScene


class TimelineCanvasView(Qw.QGraphicsView):
    sig_viewing_timeframe_changed = Qc.Signal(float, float)
    sig_zoomed = Qc.Signal(float)

    def __init__(self,
                 atm: ActionManager,
                 height: int,
                 orig_time_resolution: float,
                 time_gauge_height: int,
                 scene: TimelineCanvasScene, parent):
        super().__init__(parent)
        self.atm = atm
        self.scene: typing.Final[TimelineCanvasScene] = scene
        self.setScene(scene)
        self.height: typing.Final[int] = height
        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.time_gauge_height: typing.Final[int] = time_gauge_height
        self._top_action_top_left_x = 0
        self._top_action_top_left_y = 0
        self._visible_actions = set()
        self.setSceneRect(0, 0, self.viewport().width(),
                          self.viewport().height())
        self.setVerticalScrollBarPolicy(Qc.Qt.ScrollBarAlwaysOff)
        self.scene.changed.connect(self.scene_changed)

        self.horizontalScrollBar().rangeChanged.connect(self.move_scrollbar_to_end)
        self.horizontalScrollBar().valueChanged.connect(self.hor_slider_moved)
        self.horizontalScrollBar().sliderMoved.connect(self.hor_slider_moved)
        self._hor_slider_snap = True

        self.setAlignment(Qc.Qt.AlignTop | Qc.Qt.AlignTop)
        self._latest_viewing_scene_x_frame = (0.0, float('inf'))

        # Zoom
        self._zoom_level = 1.0
        self.target_scene_pos = Qc.QPointF()
        self.target_viewport_pos = Qc.QPointF()
        self.setMouseTracking(True)
        self.update()

    @Qc.Slot(int, int)
    def hor_slider_moved(self, pos: int):
        self._hor_slider_snap = pos == self.horizontalScrollBar().maximum()

    @Qc.Slot(int, int)
    def move_scrollbar_to_end(self, min: int, max: int):
        if self._hor_slider_snap:
            self.horizontalScrollBar().setValue(max)

    @Qc.Slot(Qc.QRect)
    def scene_changed(self, rect: Qc.QRect):
        self.setSceneRect(Qc.QRectF(0,
                                    self._top_action_top_left_y,
                                    self.scene.width(),
                                    self.viewport().height()))
        self.setAlignment(Qc.Qt.AlignTop | Qc.Qt.AlignTop)
        self.update()

    @Qc.Slot(list)
    def set_visible_actions(self, visible_actions: typing.List[str]):
        self._visible_actions = visible_actions
        self.update()

    @Qc.Slot(int)
    def vertical_slider_move(self, pos: int):
        self._top_action_top_left_y = self.height * pos
        self.setSceneRect(Qc.QRectF(0,
                                    self._top_action_top_left_y,
                                    self.scene.width(),
                                    self.viewport().height()))

    def mouseMoveEvent(self, event: Qg.QMouseEvent) -> None:
        delta = self.target_viewport_pos - event.pos()
        if Qc.qAbs(delta.x()) > 5:
            self.target_viewport_pos = Qc.QPointF(event.pos())
            self.target_scene_pos = self.mapToScene(event.pos())
            # self.scene.update(self.mapToScene(self.viewport().geometry()).boundingRect())

    def wheelEvent(self, event: Qg.QWheelEvent) -> None:
        if event.orientation() == Qc.Qt.Vertical:
            angle = event.angleDelta().y()
            self._zoom_level += angle / 1000
            self._zoom_level = min(max(self._zoom_level, 0.5), 4.0)
            self.gentle_zoom(self._zoom_level)
            # self.scene.update(self.mapToScene(self.viewport().geometry()).boundingRect())

    def keyPressEvent(self, event: Qg.QKeyEvent) -> None:
        if event.key() == Qc.Qt.Key_R:
            self._zoom_level = 1.0
            self.gentle_zoom(self._zoom_level)
            # self.scene.update(self.mapToScene(self.viewport().geometry()).boundingRect())

    def gentle_zoom(self, factor: float):
        new_time_resolution = self.orig_time_resolution * factor
        diff_mult = new_time_resolution / self.scene._time_resolution
        new_x = self.target_scene_pos.x() * diff_mult
        self.target_scene_pos.setX(new_x)
        new_x = new_x - self.target_viewport_pos.x() + (self.viewport().width() / 2.0)
        viewport_center = Qc.QPointF(new_x, self.target_scene_pos.y())
        self.sig_zoomed.emit(new_time_resolution)
        self.centerOn(viewport_center.toPoint())
        self.update()

    def drawBackground(self, painter: Qg.QPainter, rect: Qc.QRectF) -> None:
        left_x = -self.viewportTransform().dx()
        right_x = left_x + self.viewport().rect().width()
        height = 2 * rect.height()

        if self.atm.init_time:
            left_time = (left_x / self.scene._time_resolution) + self.atm.init_time
            right_time = (right_x / self.scene._time_resolution) + self.atm.init_time
            if self._latest_viewing_scene_x_frame != (left_x, right_x):
                self._latest_viewing_scene_x_frame = (left_x, right_x)
                self.sig_viewing_timeframe_changed.emit(left_time, right_time)

            cur_time = int(math.ceil(left_time))
            while cur_time <= right_time:
                if cur_time % 5 == 0:
                    pfine = Qg.QPen(Qg.QColor(100, 100, 100, 128), 3)
                else:
                    pfine = Qg.QPen(Qg.QColor(128, 128, 128, 128), 1)
                painter.setPen(pfine)
                cur_index = (cur_time - self.atm.init_time) * self.scene._time_resolution
                painter.drawLine(Qc.QLineF(cur_index, self.time_gauge_height, cur_index, height))
                cur_time = cur_time + 1

        super().drawBackground(painter, rect)
