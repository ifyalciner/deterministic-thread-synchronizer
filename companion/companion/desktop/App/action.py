#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtGui as Qg
import PySide2.QtWidgets as Qw

from companion.core.tree import Action
from . import action_instance
from . import action_window
from . import graphics_data


class ActionGOSigEmitter(Qc.QObject):
    sig_open_action_window = Qc.Signal(str)

    def __init__(self, parent):
        super().__init__()


class ActionGO(Qw.QGraphicsRectItem):
    def __init__(self,
                 gd: graphics_data.GraphicsData,
                 action: Action,
                 height: int,
                 orig_time_resolution: float,
                 orig_pos: Qc.QPointF,
                 parent):
        super().__init__(0.0, 0.0, 20.0, float(height), parent=parent)
        self.setZValue(1)
        self.sig_emitter = ActionGOSigEmitter(self)

        self.gd = gd
        self.name: typing.Final[str] = action.name
        self.action: typing.Final[Action] = action
        self.height: typing.Final[int] = height

        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.orig_pos: typing.Final[Qc.QPointF] = orig_pos
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)

        self.margin: typing.Final[int] = 15

        self._last_update = action.created_at

        self.instance_go: typing.Dict[int, action_instance.ActionInstanceGO] = dict()

        self.action_window: typing.Optional[action_window.ActionWindow] = None

        self._bk_color = Qg.QColor(Qc.Qt.lightGray)

    def update_time_resolution(self, time_resolution: float):
        self.prepareGeometryChange()
        self._time_resolution = time_resolution
        self.setPos((self.orig_pos.x() / self.orig_time_resolution) * self._time_resolution, self.pos().y())
        self.update_timeframe(self._last_update)
        for _, inst in self.instance_go.items():
            inst.update_time_resolution(self._time_resolution)

    def update_timeframe(self, end: float):
        if len(self.action.instance_order) > 0:
            last_action = self.action.instances[self.action.instance_order[-1]]
            self.prepareGeometryChange()
            self._last_update = end
            width = (min(last_action.destroyed_at, end) - self.action.created_at) * self._time_resolution
            width = max(width, 0)
            self.setRect(0, 0, width, self.height)
            for _, inst in self.instance_go.items():
                inst.update_timeframe(end)

    def open_window(self):
        if self.action_window is None:
            self.action_window = action_window.ActionWindow(self.gd, self.action)
            self.action_window.sig_closing.connect(self.closing_window)
            self.action_window.show()
        else:
            self.action_window.setFocus()
            self.action_window.activateWindow()
            self.action_window.show()

    @Qc.Slot()
    def closing_window(self):
        self.action_window = None

    def set_background_color(self, color: Qg.QColor):
        self._bk_color = color

    def paint(self, painter: Qg.QPainter, option: Qw.QStyleOptionGraphicsItem,
              widget: typing.Optional[Qw.QWidget] = ...) -> None:
        painter.fillRect(self.rect(), self._bk_color)

    def mouseDoubleClickEvent(self, event: Qw.QGraphicsSceneMouseEvent) -> None:
        self.open_window()
