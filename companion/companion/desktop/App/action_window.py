#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtGui as Qg
import PySide2.QtWidgets as Qw

from companion.core.tree import Action, ActionInstance
from companion.core.utils import get_formatted_local_time_str
from . import graphics_data


class ActionWindow(Qw.QMainWindow):
    sig_closing = Qc.Signal()

    def __init__(self, gd: graphics_data.GraphicsData, action: Action):
        super().__init__()
        self.action: typing.Final[Action] = action
        self.children_instance_list_data: typing.Tuple[ActionInstance] = []
        self.gd: graphics_data.GraphicsData = gd

        self.setupUi()
        self.show()

    def setupUi(self):
        self.setObjectName(self.action.name)
        self.setMinimumSize(Qc.QSize(551, 610))
        self.verticalLayoutWidget = Qw.QWidget(self)
        self.verticalLayoutWidget.setGeometry(Qc.QRect(9, 9, 531, 591))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayoutWidget.setSizePolicy(Qw.QSizePolicy.Expanding, Qw.QSizePolicy.Expanding)
        self.setCentralWidget(self.verticalLayoutWidget)

        self.verticalLayout = Qw.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout.setStretch(0, 1)
        self.verticalLayout.setStretch(1, 1)

        self.formLayout = Qw.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.verticalLayout.addLayout(self.formLayout)

        self.nameLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.nameLabel.setObjectName("nameLabel")
        self.formLayout.setWidget(0, Qw.QFormLayout.LabelRole, self.nameLabel)
        self.nameValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.nameValueLabel.setObjectName("nameLineEdit")
        self.nameValueLabel.setText(self.action.name)
        self.nameValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(0, Qw.QFormLayout.FieldRole, self.nameValueLabel)

        self.parentLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.parentLabel.setObjectName("parentLabel")
        self.formLayout.setWidget(1, Qw.QFormLayout.LabelRole, self.parentLabel)
        self.parentValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.parentValueLabel.setObjectName("parentLineEdit")
        self.parentValueLabel.setTextFormat(Qc.Qt.RichText)
        self.parentValueLabel.setText("<u>" + str(self.action.parent_name) + "</u>")
        self.parentValueLabel.mouseDoubleClickEvent = \
            lambda x: self.openActionWindow(self.action.parent_name)
        self.formLayout.setWidget(1, Qw.QFormLayout.FieldRole, self.parentValueLabel)

        self.typeLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.typeLabel.setObjectName("typeLabel")
        self.formLayout.setWidget(2, Qw.QFormLayout.LabelRole, self.typeLabel)
        self.typeValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.typeValueLabel.setObjectName("typeLineEdit")
        self.typeValueLabel.setText(str(self.action.type))
        self.typeValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(2, Qw.QFormLayout.FieldRole, self.typeValueLabel)

        self.createdAtLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.createdAtLabel.setObjectName("createdAtLabel")
        self.formLayout.setWidget(3, Qw.QFormLayout.LabelRole, self.createdAtLabel)
        self.createdAtValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.createdAtValueLabel.setObjectName("createdAtLineEdit")
        self.createdAtValueLabel.setText(get_formatted_local_time_str(self.action.created_at))
        self.createdAtValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(3, Qw.QFormLayout.FieldRole, self.createdAtValueLabel)

        self.destroyedAtLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.destroyedAtLabel.setObjectName("destroyedAtLabel")
        self.formLayout.setWidget(4, Qw.QFormLayout.LabelRole, self.destroyedAtLabel)
        self.destroyedAtValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.destroyedAtValueLabel.setObjectName("destroyedAtLineEdit")
        destroyed_at = self.action.instances[self.action.instance_order[-1]].destroyed_at
        if destroyed_at == float('inf'):
            destroyed_at_str = ""
        else:
            destroyed_at_str = get_formatted_local_time_str(destroyed_at)
        self.destroyedAtValueLabel.setText(destroyed_at_str)
        self.destroyedAtValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(4, Qw.QFormLayout.FieldRole, self.destroyedAtValueLabel)

        self.noChildrenLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.noChildrenLabel.setObjectName("noChildrenLabel")
        self.formLayout.setWidget(5, Qw.QFormLayout.LabelRole, self.noChildrenLabel)
        self.noChildrenValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.noChildrenValueLabel.setObjectName("noChildrenLineEdit")
        self.noChildrenValueLabel.setText(str(len(self.action.children)))
        self.noChildrenValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(5, Qw.QFormLayout.FieldRole, self.noChildrenValueLabel)

        self.noInstancesLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.noInstancesLabel.setObjectName("noInstancesLabel")
        self.formLayout.setWidget(6, Qw.QFormLayout.LabelRole, self.noInstancesLabel)
        self.noInstancesValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.noInstancesValueLabel.setObjectName("noInstancesLineEdit")
        self.noInstancesValueLabel.setText(str(len(self.action.instances)))
        self.noInstancesValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(6, Qw.QFormLayout.FieldRole, self.noInstancesValueLabel)

        self.act_inst_label = Qw.QLabel(self.verticalLayoutWidget)
        self.act_inst_label.setObjectName("logs_label")
        self.verticalLayout.addWidget(self.act_inst_label)
        # create new member called log_table with two columns as QTableWidget with 0 rows
        self.act_inst_table = Qw.QTableWidget(0, 3, self.verticalLayoutWidget)
        self.act_inst_table.setObjectName("log_table")
        self.act_inst_table.setHorizontalHeaderLabels(["Created", "Died", "Object"])
        self.act_inst_table.setColumnWidth(0, 20)
        self.act_inst_table.setColumnWidth(1, 20)
        self.act_inst_table.setColumnWidth(2, 200)
        self.act_inst_table.horizontalHeader().setStretchLastSection(True)
        self.act_inst_table.horizontalHeader().setSectionResizeMode(Qw.QHeaderView.Stretch)
        self.act_inst_table.setEditTriggers(Qw.QAbstractItemView.NoEditTriggers)
        self.act_inst_table.setSelectionBehavior(Qw.QAbstractItemView.SelectRows)
        self.act_inst_table.setSelectionMode(Qw.QAbstractItemView.SingleSelection)
        self.act_inst_table.setShowGrid(False)
        self.act_inst_table.verticalHeader().setVisible(False)
        self.act_inst_table.setAlternatingRowColors(True)
        self.act_inst_table.setSortingEnabled(False)
        self.act_inst_table.setWordWrap(False)
        self.act_inst_table.setCornerButtonEnabled(False)
        self.act_inst_table.horizontalHeader().setSectionResizeMode(Qw.QHeaderView.ResizeToContents)
        self.act_inst_table.doubleClicked.connect(self.act_inst_double_click)

        self.verticalLayout.addWidget(self.act_inst_table)

        self.retranslateUi()
        self.update()
        Qc.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        _translate = Qc.QCoreApplication.translate
        self.setWindowTitle(_translate(self.action.name, f"Action {self.action.name}"))
        self.nameLabel.setText(_translate(self.action.name, "Name"))
        self.parentLabel.setText(_translate(self.action.name, "Parent"))
        self.typeLabel.setText(_translate(self.action.name, "Type"))
        self.createdAtLabel.setText(_translate(self.action.name, "Created at"))
        self.destroyedAtLabel.setText(_translate(self.action.name, "Destroyed at"))
        self.noChildrenLabel.setText(_translate(self.action.name, "# children"))
        self.noInstancesLabel.setText(_translate(self.action.name, "# instances"))
        self.act_inst_label.setText(_translate(self.action.name, "Children and instances"))

    def update(self):
        """
        Here we update following fields:
            * destroyed at
            * no children
            * no instances
            * children list
            * instance list
        """

        self.children_instance_list_data = [(instance, f"Instance [{instance.id}]") for instance
                                            in self.action.instances.values()]
        for instance, _ in self.children_instance_list_data.copy():
            for child in instance.children.values():
                self.children_instance_list_data.append(
                    (child, f"Child \"{child.name}\" [{child.id}] of [{instance.id}]"))

        self.children_instance_list_data.sort(key=lambda x: x[0].created_at)

        if self.act_inst_table.rowCount() != len(self.children_instance_list_data):
            # clean act_inst_table
            self.act_inst_table.setRowCount(0)
            # add all members of self.children_instance_list_data to act_inst_table
            for item, label in self.children_instance_list_data:
                self.act_inst_table.insertRow(self.act_inst_table.rowCount())
                self.act_inst_table.setItem(self.act_inst_table.rowCount() - 1, 0,
                                            Qw.QTableWidgetItem(get_formatted_local_time_str(item.created_at)))
                self.act_inst_table.setItem(self.act_inst_table.rowCount() - 1, 1,
                                            Qw.QTableWidgetItem(get_formatted_local_time_str(
                                                item.destroyed_at) if item.destroyed_at != float('inf') else ""))
                self.act_inst_table.setItem(self.act_inst_table.rowCount() - 1, 2, Qw.QTableWidgetItem(label))

        destroyed_at = self.action.instances[self.action.instance_order[-1]].destroyed_at
        if destroyed_at != float('inf'):
            if self.destroyedAtValueLabel.text() != "":
                destroyed_at_str = get_formatted_local_time_str(destroyed_at)
                self.destroyedAtValueLabel.setText(destroyed_at_str)

        self.noInstancesValueLabel.setText(str(len(self.action.instances)))
        self.noChildrenValueLabel.setText(str(len(self.action.children)))

    def closeEvent(self, event: Qg.QCloseEvent) -> None:
        self.sig_closing.emit()
        super().closeEvent(event)

    def openInstanceWindow(self, instance_id: int, instance_name: str = None):
        if instance_name is None:
            # find instance_name by instance_id
            for name, instance in self.gd.action_graphic_objects.items():
                if instance_id in instance.instance_go:
                    instance_name = name
                    break
        self.gd.action_graphic_objects[instance_name].instance_go[instance_id].open_window()

    def openActionWindow(self, action_name: str):
        self.gd.action_graphic_objects[action_name].open_window()

    @Qc.Slot(Qw.QTableWidgetItem)
    def act_inst_double_click(self, item: Qw.QTableWidgetItem):
        instance: ActionInstance = self.children_instance_list_data[item.row()][0]
        self.openInstanceWindow(instance.id, instance.name)
