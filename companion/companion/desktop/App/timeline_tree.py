#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtWidgets as Qw

from companion.core import ActionManager


class TimelineTreeItem(Qw.QTreeWidgetItem):
    def __init__(self, parent, name: str):
        super().__init__(parent, [name])
        self.name: typing.Final = name


class TimelineTree(Qw.QTreeWidget):
    sig_visible_actions_changed: Qc.Signal = Qc.Signal(list)

    def __init__(self, atm: ActionManager, parent):
        super().__init__(parent)
        self._tree_items: typing.Dict[str, Qw.QTreeWidgetItem] = dict()
        self.atm: typing.Final[ActionManager] = atm

        # list of time and event
        self._user_commands = []

        self._current_visible: typing.Set[str] = {""}
        self.itemExpanded.connect(self.handle_visible_change_expand)
        self.itemCollapsed.connect(self.handle_visible_change_collapse)

        self._visible_timeframe: typing.Tuple[float, float] = (0, float("inf"))
        self._row_height = 0

        self._collapsed: typing.Set[str] = set()

        self.setMinimumSize(Qc.QSize(100, 500))
        self.setHeaderHidden(True)
        self.setObjectName("treeWidget")
        self.setContentsMargins(0, 0, 0, 0)
        self.setStyleSheet("""
                /* https://doc.qt.io/qt-6/stylesheet-examples.html */
                /*
                QTreeView::branch {
                        background: palette(base);
                }

                QTreeView::branch:has-siblings:!adjoins-item {
                        background: cyan;
                }

                QTreeView::branch:has-siblings:adjoins-item {
                        background: red;
                }

                QTreeView::branch:!has-children:!has-siblings:adjoins-item {
                        background: blue;
                }

                QTreeView::branch:closed:has-children:has-siblings {
                        background: pink;
                }

                QTreeView::branch:has-children:!has-siblings:closed {
                        background: gray;
                }

                QTreeView::branch:open:has-children:has-siblings {
                        background: magenta;
                }

                QTreeView::branch:open:has-children:!has-siblings {
                        background: green;
                }
                */
                
                QTreeWidget::item { border-bottom: 1px solid black;}
                
                QTreeView::branch:has-siblings:!adjoins-item {
                    border-image: url(:/icons/treeview-vline.png) 0;
                }

                QTreeView::branch:has-siblings:adjoins-item {
                    border-image: url(:/icons/treeview-branch-more.png) 0;
                }

                QTreeView::branch:!has-children:!has-siblings:adjoins-item {
                    border-image: url(:/icons/treeview-branch-end.png) 0;
                }

                QTreeView::branch:has-children:!has-siblings:closed,
                QTreeView::branch:closed:has-children:has-siblings {
                        border-image: none;
                        image: url(:/icons/treeview-branch-closed.png);
                }

                QTreeView::branch:open:has-children:!has-siblings,
                QTreeView::branch:open:has-children:has-siblings  {
                        border-image: none;
                        image: url(:/icons/treeview-branch-open.png);
                }
        """)

    @property
    def row_height(self):
        return self._row_height

    @row_height.setter
    def row_height(self, height: float):
        self._row_height = height

    def get_child_visible_actions(self, expanded_parents: typing.List[str], active: typing.Set[str]) -> typing.List[
        str]:
        ret_visibles = list()
        if len(expanded_parents) == 0:
            return list()

        for aid in expanded_parents:
            a = self._tree_items[aid]
            for child_idx in range(a.childCount()):
                child_it = a.child(child_idx)
                if child_it.name in active:
                    ret_visibles.append(child_it.name)
                    if child_it.name not in self._collapsed:
                        ret_visibles += self.get_child_visible_actions([child_it.name], active)
        return ret_visibles

    def get_expanded_from_root(self, active: typing.Set[str]) -> typing.List[str]:
        visible = []
        for idx in range(self.topLevelItemCount()):
            it = self.topLevelItem(idx)
            if it.name in active:
                visible.append(it.name)
                if it.name not in self._collapsed:
                    visible += self.get_child_visible_actions([it.name], active)
        return visible

    @Qc.Slot(float, float)
    def set_viewing_timeframe(self, frm: float, to: float):
        self._visible_timeframe = (frm, to)
        self.send_updated_visible_actions()

    @Qc.Slot(TimelineTreeItem)
    def handle_visible_change_expand(self, item: TimelineTreeItem):
        try:
            self._collapsed.remove(item.name)
        except KeyError:
            pass
        self.send_updated_visible_actions()

    @Qc.Slot(TimelineTreeItem)
    def handle_visible_change_collapse(self, item: TimelineTreeItem):
        self._collapsed.add(item.name)
        self.send_updated_visible_actions()

    def send_updated_visible_actions(self):
        active = {xname for xname in self.atm.actions if
                  self.atm.action(xname).is_any_instance_active_in_timeframe(*self._visible_timeframe)}
        expanded = self.get_expanded_from_root(active)
        for name, x in self._tree_items.items():
            x.setHidden(name not in active)
        self.sig_visible_actions_changed.emit(expanded)

    @Qc.Slot(str)
    def action_add_cb(self, action_name: str):
        action = self.atm.action(action_name)
        if action.parent_name == "":
            self._tree_items[action_name] = TimelineTreeItem(self, action.name)
            self.addTopLevelItem(self._tree_items[action_name])
        else:
            self._tree_items[action_name] = TimelineTreeItem(self._tree_items[action.parent_name], action.name)
            self._tree_items[action.parent_name].addChild(self._tree_items[action_name])

        self._tree_items[action_name].setSizeHint(0, Qc.QSize(1000, self._row_height))
        self._tree_items[action_name].setFlags(self._tree_items[action.name].flags() & ~Qc.Qt.ItemIsEditable)
        self._tree_items[action_name].setFlags(self._tree_items[action.name].flags() & ~Qc.Qt.ItemIsSelectable)
        self._tree_items[action_name].setExpanded(True)
        # self._tree_items[action.id].set

        if action.parent_name in self._current_visible:
            self.send_updated_visible_actions()

        self.update()
