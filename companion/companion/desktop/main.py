#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

# Python
import sys

# 3rd Party
import PySide2.QtWidgets as Qw

import companion.autogen.resources as resources
from App import TimelineApp, FergusonContact
# Project
from companion.core import ActionManager

if __name__ == '__main__':
    port = int(sys.argv[1])

    ## Create the Qt Application
    app = Qw.QApplication(sys.argv)
    resources

    # todo here given an ip find the running instances automatically

    main_menu_bar = Qw.QMenuBar()
    main_menu = main_menu_bar.addMenu("&Main")

    main_widget = Qw.QWidget()
    main_widget.setWindowTitle("Ferguson Companion")
    main_widget.resize(1024, 800)

    tab_widget = Qw.QTabWidget(main_widget)

    atm = ActionManager()
    space_app = TimelineApp("TimeLine", atm, tab_widget)
    fc = FergusonContact("localhost", port, atm)
    tab_widget.addTab(space_app, space_app.name)

    layout = Qw.QVBoxLayout(main_widget)
    layout.addWidget(main_menu_bar)
    layout.addWidget(tab_widget)
    layout.setContentsMargins(0, 0, 0, 0)
    layout.setSpacing(0)
    main_widget.showNormal()
    sys.exit(app.exec_())
