#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import json
import threading
import time
import typing
from dataclasses import dataclass
from enum import Enum


@dataclass
class Log:
    t: int
    frm: int
    type: int
    content: str
    file_name: str
    line_no: int

    @classmethod
    def get(message_text: str) -> 'Log':
        m = json.load(str)
        return Log(**m)


@dataclass
class Feedback:
    t: int
    from_action_id: int
    content: str


class ActionType(Enum):
    Action = 1
    YieldingAction = 2
    PolicyAction = 3


Time: typing.TypeAlias = float


@dataclass
class ActionSnapshot:
    t: Time
    action: 'ActionInstance'
    created: bool
    destroyed: bool


class ActionInstance:
    def __init__(self, t: Time, name: str, id: int, parent_id: int):
        self.name: typing.Final[str] = name
        self.id: typing.Final[int] = id
        self.parent_id: typing.Final[int] = parent_id
        self.created_at: typing.Final[float] = t

        self.children: typing.Dict[int, 'ActionInstance'] = {}

        self._lock = threading.Lock()

        self._destroyed_at = float('inf')
        self._destroyed_post_callback = []

        self.returned: typing.Optional[typing.Any] = ""

        self.all_logs: typing.List[typing.Tuple[Time, typing.Any]] = []

        self.feedbacks: typing.List[typing.Tuple[Time, typing.Any]] = []
        self._add_feedback_pre_callback = []
        self._add_feedback_post_callback = []

        self.text_logs: typing.List[typing.Tuple[Time, typing.Any]] = []
        self._add_log_pre_callback = []
        self._add_log_post_callback = []

    @property
    def destroyed_at(self) -> float:
        self._lock.acquire()
        cp = self._destroyed_at
        self._lock.release()
        return cp

    @destroyed_at.setter
    def destroyed_at(self, t: Time):
        self._lock.acquire()
        self._destroyed_at = t
        self._lock.release()
        for cb in self._destroyed_post_callback:
            cb()

    def exists(self, t: Time) -> bool:
        return t >= self.created_at

    def alive(self, t: Time) -> bool:
        return self.exists(t) and t < self._destroyed_at

    def destroyed(self, t: Time) -> bool:
        return t > self._destroyed_at

    def active_between(self, frm: float, to: float) -> bool:
        return not ((self.created_at < frm and self.destroyed_at < frm) or (
                self.created_at > to and self._destroyed_at > to))

    def get_snapshot(self, t: Time) -> ActionSnapshot:
        return ActionSnapshot(t, self, time > self.created_at, t > self._destroyed_at)

    def add_log(self, log: Log):
        ltime = log['time']
        time_str = str(time.strftime("%H:%M:%S", time.localtime(ltime)))
        if log['type'] == '_fb_r':
            content = log['content']
            # self._feedbacks.append((ltime, content))
            self.feedbacks.append((ltime, content))
            self.all_logs.append((ltime, content))
            for cb in self._add_feedback_post_callback:
                cb(self.id, len(self.feedbacks) - 1)
                # cb(ltime, content)
            return
        elif log['type'] == '_t':
            self.text_logs.append((ltime, f"{time_str} - {str(log)}"))
            self.all_logs.append((ltime, f"{time_str} - {str(log)}"))
        else:
            self.text_logs.append((ltime, f"{time_str} - {str(log)}"))
            self.all_logs.append((ltime, f"{time_str} - {str(log)}"))
        # self._logs.append((time, content))
        for cb in self._add_log_post_callback:
            cb(self.id, len(self.text_logs) - 1)
            # cb(ltime, log['content'])

    def add_destroyed_post_callbacks(self, cb: typing.Callable):
        self._destroyed_post_callback.append(cb)

    def add_feedback_post_callbacks(self, cb: typing.Callable):
        self._add_feedback_post_callback.append(cb)

    def add_log_post_callbacks(self, cb: typing.Callable):
        self._add_log_post_callback.append(cb)


class Action:
    def __init__(self, name: str, parent_name: str, atype: int, created_at: float):
        self.name: typing.Final[str] = name
        self.parent_name: typing.Final[str] = parent_name
        self.type: typing.Final[str] = atype
        self.created_at: typing.Final[float] = created_at
        self._children: typing.Dict[str, 'Action'] = dict()
        self._instances: typing.Dict[int, ActionInstance] = dict()
        self._instance_order: typing.List[int] = []

        self._lock = threading.Lock()
        self._add_instance_post_callback = []
        self._add_child_post_callback = []

    def add_instance(self, act: ActionInstance):
        self._lock.acquire()
        self._instances[act.id] = act
        self._instance_order.append(act.id)
        self._lock.release()
        for cb in self._add_instance_post_callback:
            cb(act.id)

    # method add children
    def add_child(self, act: 'Action'):
        self._lock.acquire()
        self._children[act.name] = act
        self._lock.release()
        for cb in self._add_child_post_callback:
            cb(act.name)

    @property
    def instances(self) -> typing.Dict[int, ActionInstance]:
        return self._instances.copy()

    @property
    def instance_order(self) -> typing.List[int]:
        return self._instance_order

    @property
    def children(self) -> typing.Dict[str, 'Action']:
        return self._children.copy()

    def add_new_instance_post_callbacks(self, cb: typing.Callable):
        self._add_instance_post_callback.append(cb)

    def add_new_child_post_callbacks(self, cb: typing.Callable):
        self._add_child_post_callback.append(cb)

    def is_any_instance_active_in_timeframe(self, frm: float, to: float) -> bool:
        return any([inst.active_between(frm, to) for _, inst in self._instances.items()])


# class Policy:
#    id: int
#    content: str
#
#
# class PolicyAction(Action):
#    def __int__(self, id: int, parent_id: int, name: str, goal: str):
#        super().__init__(id, parent_id, name, goal, ActionType.PolicyAction)
#        self.cur_state: str = None
#        self.policy_list: Policy = None
#        self.state_history: typing.List[str] = []
#        self.policy_history: typing.List[Policy] = []


class ActionManager:
    def __init__(self):
        ActionTree: typing.TypeAlias = typing.Set[int]
        # All existing actions created so far

        self._lock = threading.Lock()

        self._actions: typing.Dict[str, Action] = dict()
        self._instances: typing.Dict[int, ActionInstance] = dict()

        self._all_logs = []
        self._init_time = None
        self._cur_time = None

        self._action_added_post_cb = []
        # self._instance_added_post_cb = []
        # self._instance_destroyed_post_cb = []
        self._process_log_post_cb = []
        self._time_progress_post_cb = []

    @property
    def init_time(self) -> typing.Optional[float]:
        return self._init_time

    @property
    def cur_time(self) -> typing.Optional[float]:
        return self._cur_time

    @property
    def actions(self) -> typing.List[str]:
        return self._actions.keys()

    def action(self, name: str) -> Action:
        return self._actions[name]

    def instance(self, iid: int) -> ActionInstance:
        return self._instances[iid]

    def add_action_added_post_cb(self, cb: typing.Callable):
        self._action_added_post_cb.append(cb)

    def add_instance_added_post_cb(self, cb: typing.Callable):
        self._instance_added_post_cb.append(cb)

    # def add_action_destroyed_post_cb(self, cb: typing.Callable):
    #    self._lock.acquire()
    #    self._action_destroyed_post_cb.append(cb)
    #    self._lock.release()

    def add_time_progress_post_cb(self, cb: typing.Callable):
        self._lock.acquire()
        self._time_progress_post_cb.append(cb)
        self._lock.release()

    def process_logs(self, logs):
        """
        Log format:

        """
        if not isinstance(logs, list):
            logs = [logs]

        created_actions: typing.List[str] = list()  # need to be in order incase child and parent are created together
        created_instances: typing.List[int] = list()  # need to be in order incase child and parent are created together
        updated_actions: typing.Set[str] = set()
        updated_instances: typing.Set[int] = set()
        destroyed_instances: typing.List[
            int] = list()  # need to be in order incase child and parent are destroyed together

        self._lock.acquire()
        for l in logs:
            if not self._init_time:
                self._init_time = l['time']
            self._all_logs.append((l['time'], l))
            self._cur_time = l['time']
            time_str = str(time.strftime("%H:%M:%S", time.localtime(self._cur_time)))
            print(f"{time_str} - {l}")

            aid = int(l['from'])
            if l['type'] == '_ac_c':
                content = l['content']

                a_inst = ActionInstance(t=l['time'],
                                        name=content['name'],
                                        id=content['id'],
                                        parent_id=content['parent_id'])
                if a_inst.parent_id != 0:
                    self._instances[a_inst.parent_id].children[a_inst.id] = a_inst
                created_instances.append((a_inst.name, a_inst.id))
                self._instances[a_inst.id] = a_inst
                if content['name'] not in self._actions.keys():
                    action = Action(name=a_inst.name,
                                    parent_name="" if a_inst.parent_id == 0 else self._instances[a_inst.parent_id].name,
                                    atype=content['type'],
                                    created_at=l['time'])
                    created_actions.append(action.name)
                    self._actions[action.name] = action
                    for cb in self._action_added_post_cb:
                        cb(action.name)

                self._actions[a_inst.name].add_instance(a_inst)
                updated_actions.add(a_inst.name)

            elif l['type'] in {'_ac_r', '_ac_f', '_ac_p', '_ac_t'}:
                destroyed_instances.append(aid)
                self._instances[aid].destroyed_at = l['time']
            else:
                updated_instances.add(aid)
                self._instances[aid].add_log(l)
        self._lock.release()

        # for action in created_actions:
        #    for cb in self._action_added_post_cb:
        #        cb(action)

        # for action_name, instance_id in created_instances:
        #    for cb in self._action_added_post_cb:
        #        cb(action_name, instance_id)

        # for a_inst in created_actions:
        #    for cb in self._action_added_post_cb:
        #        cb(self._actions[a_inst])
        # for u in updated_actions:
        #    for cb in self._process_log_post_cb:
        #        cb(self._instances[a_inst])
        # for d in destroyed_actions:
        #    for cb in self._action_destroyed_post_cb:
        #        cb(self._instances[a_inst])
        for cb in self._time_progress_post_cb:
            cb(self)

    @classmethod
    def __get_validators__(cls):
        """ See pydantic doc. https://pydantic-docs.helpmanual.io/usage/types/#custom-data-types
        """
        yield cls.validate

    @classmethod
    def validate(cls, v):
        """ See pydantic doc. https://pydantic-docs.helpmanual.io/usage/types/#custom-data-types
        """
        assert isinstance(v, cls)
        return v
