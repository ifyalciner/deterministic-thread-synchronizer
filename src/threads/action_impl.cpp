// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

/* Ferguson */
#include "action_impl.h"
#include "action_manager.h"

/* CPP */
#include <exception>
#include <iostream>
#include <sstream>
#include <utility>

/* Boost */
#include <boost/filesystem.hpp>

namespace Ferguson::Threads {

//boost::property_tree::ptree
//ActionProduct::to_json() const {
//  boost::property_tree::ptree pt;
//  return pt;
//}
//
//boost::property_tree::ptree
//ActionResult::to_json() const {
//  std::string str("null");
//  if(exception) {
//    try {
//      std::rethrow_exception(exception);
//    }
//    catch(std::exception &e) {
//      str = e.what();
//    }
//  }
//  boost::property_tree::ptree pt;
//  pt.put("exception", str);
//  return pt;
//}

/**********************************************************************************************************************/
/* ActionImpl */

ActionImpl::ActionImpl(ActionID parent,                    //
                       TaskBaseSPtr task,                  //
                       std::string name,                   //
                       std::string task_class_name,        //
                       TimeUnit const &max_terminate_delay,//
                       std::optional<ActionProductCB> product_callback)
    : _task(std::move(task)),                        //
      _id(ActionManager::getID()),                   //
      _parent(parent.id),                            //
      _name(std::move(name)),                        //
      _task_class_name(std::move(task_class_name)),  //
      _product_callback(std::move(product_callback)),//
      _manager(ActionManager::get()),                //
      _host_sync(nullptr),                           //
      _thread_sync(nullptr),                         //
      _task_thread_handle_sptr(nullptr),             //
      _host_thread_running(false),                   //
      _host_thread_handle_sptr(nullptr) {

  std::tie(_host_sync, _thread_sync) = Synchronizer::anytimeFactory(max_terminate_delay);
  _manager->addAction(this);

  _task_thread_handle_sptr = std::make_shared<boost::thread>([this] { executeRun(); });
  if(_product_callback) {
    _host_thread_running     = true;
    _host_thread_handle_sptr = std::make_shared<boost::thread>([this] { callback_handler(); });
  }
  _host_sync->requestInitialize();
}

ActionImpl::~ActionImpl() {
  // If host hasn't done it yet, we will
  _host_sync->requestTerminate();
  if(_task_thread_handle_sptr) {
    _task_thread_handle_sptr->interrupt();
    _host_sync->waitTerminated();
    if(_task_thread_handle_sptr && _task_thread_handle_sptr->joinable()) {
      _task_thread_handle_sptr->join();
    }
    _task_thread_handle_sptr = nullptr;
  }
  if(_host_thread_handle_sptr) {
    _host_thread_handle_sptr->interrupt();
    if(_host_thread_handle_sptr->joinable()) {
      _host_thread_handle_sptr->join();
    }
    _host_thread_handle_sptr = nullptr;
  }
  _manager->removeAction(id());
}

void
ActionImpl::preempt() {
  _host_sync->requestTerminate();
  _task_thread_handle_sptr->interrupt();
  _host_thread_handle_sptr->interrupt();
}

bool
ActionImpl::finished() {
  return _host_sync->isTerminated() && !bool(_host_thread_running);
}

void
ActionImpl::waitUntilFinished(TimeUnit timeout) {
  _host_sync->waitTerminated(timeout);
}

std::any
ActionImpl::getProduct() {
  return _host_sync->getProduct();
}

void
ActionImpl::throwException() {
  auto exception = _host_sync->threadException();
  if(exception) {
    std::rethrow_exception(exception);
  }
}

ActionIDType
ActionImpl::id() const {
  return _id;
}

ActionIDType
ActionImpl::parent() const {
  return _parent;
}

std::shared_ptr<ActionImpl>
ActionImpl::getptr() {
  return shared_from_this();
}

std::shared_ptr<const ActionImpl>
ActionImpl::getcptr() const {
  return std::static_pointer_cast<const ActionImpl>(shared_from_this());
}

//boost::property_tree::ptree
//ActionImpl::getHeader(std::string const &file_name,//
//                      uint64_t line_no) {
//  // use boost filesystem to convert file_name to canonical path
//  // Create boost property tree
//  boost::property_tree::ptree pt;
//  pt.put("file", boost::filesystem::canonical(boost::filesystem::path(file_name)).string());
//  pt.put("line", line_no);
//  pt.put("id", _id);
//  pt.put("pid", _parent);
//  pt.put("action_name", _name);
//  pt.put("action_class", _task_class_name);
//  {
//    // Convert time_point to double from epoch
//    auto time_point                       = Ferguson::Clock::now();
//    auto time_since_epoch                 = time_point.time_since_epoch();
//    auto time_since_epoch_in_seconds      = std::chrono::duration_cast<std::chrono::seconds>(time_since_epoch);
//    auto time_since_epoch_in_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(time_since_epoch);
//    double time_since_epoch_in_seconds_double =
//        double(time_since_epoch_in_seconds.count()) + double(time_since_epoch_in_milliseconds.count() % 1000) / 1000.0;
//    pt.put("timestamp", time_since_epoch_in_seconds_double);
//  }
//  return pt;
//}
//
//void
//ActionImpl::logMessage(std::string const &message,  //
//                       std::string const &file_name,//
//                       uint64_t line_no,            //
//                       uint64_t mode) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "LOG");
//  json_ptree.put("message", message);
//  logUser(json_ptree, mode);
//}
//
//void
//ActionImpl::logMessage(boost::property_tree::ptree const &message,//
//                       std::string const &file_name,              //
//                       uint64_t line_no,                          //
//                       uint64_t mode) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "LOG");
//  json_ptree.add_child("message", message);
//  logUser(json_ptree, mode);
//}
//
//void
//ActionImpl::logProductSent(ActionProductCSPtr const &product,//
//                            std::string const &file_name,       //
//                            uint64_t line_no) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "FEEDBACK_SENT");
//  json_ptree.add_child("product", product->to_json());
//  logUser(json_ptree, LogMode::INFO);
//}
//
//void
//ActionImpl::logResultSent(const ActionResultCSPtr &result,//
//                          std::string const &file_name,   //
//                          uint64_t line_no) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "RESULT_SENT");
//  json_ptree.add_child("product", result->to_json());
//  logUser(json_ptree, LogMode::INFO);
//}
//
//void
//ActionImpl::logProductReceived(const ActionProductCSPtr &product,//
//                                uint64_t from,                      //
//                                std::string const &file_name,       //
//                                uint64_t line_no) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "FEEDBACK_RECEIVED");
//  json_ptree.put("from", from);
//  json_ptree.add_child("product", product->to_json());
//  logUser(json_ptree, LogMode::INFO);
//}
//
//void
//ActionImpl::logResultReceived(ActionResultCSPtr const &result,//
//                              uint64_t from,                  //
//                              std::string const &file_name,   //
//                              uint64_t line_no) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "RESULT_RECEIVED");
//  json_ptree.put("from", from);
//  json_ptree.add_child("result", result->to_json());
//  logUser(json_ptree, LogMode::INFO);
//}
//
//void
//ActionImpl::logUser(boost::property_tree::ptree const &pt,//
//                    uint64_t mode) {
//  std::stringstream ss;
//  boost::property_tree::write_json(ss, pt, false);
//  std::string message = ss.str();
//  if(mode & LogMode::INFO) {
//    std::cout << "Info: " << message << std::endl;
//  }
//  else if(mode & LogMode::WARNING) {
//    std::cout << "\\033[1;33mWarning:\\033[0m " << message << std::endl;
//  }
//  else if(mode & LogMode::ERROR) {
//    std::cerr << "\\033[1;31mError: \\033[0m " << message << std::endl;
//  }
//  else if(mode & LogMode::FATAL) {
//    std::cerr << "\\033[31;2mmFatal: \\033[0m " << message << std::endl;
//  }
//  else {
//    std::cout << message << std::endl;
//  }
//}
//
//void
//ActionImpl::logCreated(std::string const &file_name,//
//                       uint64_t line_no) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "CREATED");
//  logUser(json_ptree, LogMode::INFO);
//}
//
//void
//ActionImpl::logDestroyed(std::string const &file_name,//
//                         uint64_t line_no) {
//  auto json_ptree = getHeader(file_name,//
//                              line_no);
//  json_ptree.put("type", "DESTROYED");
//  logUser(json_ptree, LogMode::INFO);
//}

void
ActionImpl::executeRun() noexcept {
  try {
    if(_thread_sync->waitInitializationRequest() && !_thread_sync->isTerminateRequested()) {
      _thread_sync->setInitialized();
      // Following is problematic product_wrapper input type should be what ever user would like to provide.
      auto product_wrapper = [this](std::any const &product) -> void { _thread_sync->publishProduct(product); };
      _task->run(product_wrapper);
    }
  }
  catch(Synchronizer::thread_interrupted_error const &e) {
    // We don't do anything for interruption
  }
  catch(...) {
    _thread_sync->throwException(std::current_exception());
    //logResultSent(result, __FILE__, __LINE__);
  }
  _thread_sync->setTerminated();
}

void
ActionImpl::callback_handler() noexcept {
  try {
    if(_product_callback) {
      while(!_host_sync->isTerminated()) {
        // requestTermination will have getProduct method return even if it is indefinitely waiting
        auto fb = _host_sync->getProduct();
        if(fb.has_value()) {
          _product_callback.value()(fb);
        }
      }
    }
  }
  catch(boost::thread_interrupted const &e) {
    // We don't do anything for interruption
  }
  _host_thread_running = false;
}

ActionSPtr
factory(ActionID parent,
        TaskBaseSPtr t,                     //
        std::string const &instance_name,   //
        std::string const &task_class_name, //
        TimeUnit const &max_terminate_delay,//
        std::optional<ActionProductCB> const &product_cb) {
  return std::make_shared<ActionImpl>(parent,             //
                                      std::move(t),       //
                                      instance_name,      //
                                      task_class_name,    //
                                      max_terminate_delay,//
                                      product_cb);
}

}// namespace Ferguson::Threads
