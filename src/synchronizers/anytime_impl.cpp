// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "anytime_impl.h"

namespace Ferguson::Synchronizer {

AnytimeImpl::AnytimeImpl(TimeUnit const &max_terminate_delay)
    : InterruptableImpl(max_terminate_delay),//
      _product_queue() {}

std::any
AnytimeImpl::getProduct(TimePoint deadline) {
  std::any ret;
  InterruptableImpl::hostSafeExecute(
      [&]() {
        if(!_product_queue.empty()) {
          ret = _product_queue.front();
          _product_queue.pop();
        }
      },                                                                                 //
      deadline,                                                                          //
      [this]() { return (InterruptableImpl::isTerminated() && _product_queue.empty()); },//
      [this]() { return !_product_queue.empty(); });
  return ret;
}

/*---------------------------------------------------------------*/
void
AnytimeImpl::publishProduct(std::any feedback) {
  InterruptableImpl::threadSafeExecute([this, &feedback]() { _product_queue.emplace(feedback); },//
                                       TimePoint::max(),                                         //
                                       ALWAYS_FALSE,                                             //
                                       ALWAYS_TRUE);
}

/*---------------------------------------------------------------*/
std::pair<AnytimeHostSPtr, AnytimeThreadSPtr>
anytimeFactory(TimeUnit const &max_terminate_delay) {
  auto impl = std::make_shared<AnytimeImpl>(max_terminate_delay);
  return std::make_pair(std::dynamic_pointer_cast<AnytimeHost>(impl),//
                        std::dynamic_pointer_cast<AnytimeThread>(impl));
}

}// namespace Ferguson::Synchronizer
