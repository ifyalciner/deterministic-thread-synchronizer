// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

/* CPP */
#include <exception>
#include <iostream>
#include <sstream>

/* 3rd Party */
#ifdef DT_BOOST_STACK_TRACE
#include <boost/stacktrace.hpp>
#endif

/* Project */
#include <ferguson/synchronizers/interruptable.h>

/* Local */
#include "interruptable_impl.h"

namespace Ferguson::Synchronizer {

InterruptableImpl::InterruptableImpl(TimeUnit response_time)
    : SynchronizerImpl(response_time),//
      _initialize_requested(false),   //
      _initialized(false),            //
      _terminate_requested(false),    //
      _terminated(false),             //
      _last_terminate_check(Clock::now()) {
  isTerminateRequested();
}

InterruptableImpl::~InterruptableImpl() {
  // Force error throw if object is killed without any termination check during its lifetime
  try {
    isTerminateRequested();
  }
  catch(response_timeout_error const &e) {
  }
  catch(thread_interrupted_error const &e) {
  }
}
/*---------------------------------------------------------------*/
/* Host Methods */

bool
InterruptableImpl::hostSafeExecute(std::function<void()> const &procedure,//
                                   TimePoint deadline,                    //
                                   const NullaryBoolF &fail_if_true,      //
                                   const NullaryBoolF &delay_until_true) {

  // Fail if termination is requested
  return SynchronizerImpl::safeExecute(procedure,   //
                                       deadline,    //
                                       fail_if_true,//
                                       delay_until_true);
}

bool
InterruptableImpl::hostWaitUntil(TimePoint deadline,              //
                                 const NullaryBoolF &fail_if_true,//
                                 const NullaryBoolF &delay_until_true) {
  return InterruptableImpl::hostSafeExecute([]() {},     //
                                            deadline,    //
                                            fail_if_true,//
                                            delay_until_true);
}

void
InterruptableImpl::requestInitialize() {
  // Use safeExecute for implicit update called by it
  _initialize_requested = true;
  update();
}

bool
InterruptableImpl::isInitialized() {
  return _initialized;
}

bool
InterruptableImpl::waitInitialized(Ferguson::TimeUnit timeout) {
  return hostWaitUntil(
      toDeadline(timeout),              //
      [this] { return isTerminated(); },//
      [this] { return isInitialized(); });
}

void
InterruptableImpl::requestTerminate() {
  _terminate_requested = true;
  update();
}

bool
InterruptableImpl::isWaitingTermination() {
  return _terminate_requested;
}

bool
InterruptableImpl::isTerminated() {
  return _terminated;
}

bool
InterruptableImpl::waitTerminated(Ferguson::TimeUnit timeout) {
  // Warning here we don't want to use `Interruptable::InterruptableImpl::waitUntil` because of its implicit termination when
  // terminate is requested (not terminated). We would like to be still waiting after terminate is requested.
  return hostWaitUntil(toDeadline(timeout),//
                       ALWAYS_FALSE,       //
                       [this] { return isTerminated(); });
}

std::exception_ptr
InterruptableImpl::threadException() {
  std::exception_ptr exception_ptr;
  // Here we use hostSafeExecute because we are not interested in getting interrupted by termination
  hostSafeExecute([&]() { exception_ptr = _exception_ptr; },//
                  TimePoint::max(),                         //
                  ALWAYS_FALSE,                             //
                  ALWAYS_TRUE);
  return exception_ptr;
}

/*---------------------------------------------------------------*/
/* Thread Methods */

bool
InterruptableImpl::threadSafeExecute(std::function<void()> const &procedure,//
                                     TimePoint deadline,                    //
                                     const NullaryBoolF &fail_if_true,      //
                                     const NullaryBoolF &delay_until_true) {

  // All thread methods should be terminated upon receiving termination request
  return SynchronizerImpl::safeExecute(
      procedure,                                                                   //
      deadline,                                                                    //
      [this, &fail_if_true]() { return isTerminateRequested() || fail_if_true(); },//
      delay_until_true);
}

bool
InterruptableImpl::threadWaitUntil(TimePoint deadline,              //
                                   const NullaryBoolF &fail_if_true,//
                                   const NullaryBoolF &delay_until_true) {
  return InterruptableImpl::threadSafeExecute([]() {}, deadline, fail_if_true, delay_until_true);
}
bool
InterruptableImpl::isInitializeRequested() {
  // We don't check if lock is acquired because it can only fail if thread is terminated and in that
  // case the returned value is meaningless
  // No update after check is necessary
  return _initialize_requested;
}

void
InterruptableImpl::setInitialized() {
  // Use safeExecute for implicit update called by it
  _initialize_requested = true;// in case request was an implicit one
  _initialized          = true;
  update();
}

bool
InterruptableImpl::isTerminateRequested() {
  try {
#ifndef DEBUGGING
    TimeUnit diff;
    auto now = Clock::now();
    if(TimePoint(_last_terminate_check) != TimePoint()) {
      diff = boost::chrono::duration_cast<TimeUnit>(now - TimePoint(_last_terminate_check));
    }
    else {
      diff = TimeUnit(0);
    }
    _last_terminate_check = now;
    if(diff > this->_response_time) {
      std::stringstream ss;
      ss << "Delay " << diff.count() << "ms exceeds allowed max " << this->_response_time.count() << "ms!" << std::endl;
#ifdef DT_BOOST_STACK_TRACE
      // Report where we have missed checking
      ss << boost::stacktrace::stacktrace() << std::endl;
#endif
      std::clog << ss.str() << std::endl;
    }
#endif
  }
  catch(...) {
    std::exception_ptr e = std::current_exception();
    std::clog << (e ? typeid(e).name() : "null") << std::endl;
    //
  }
  // Check average time, if exceeds allowed max_delay error by 10%
  return _terminate_requested;
}

void
InterruptableImpl::setTerminated() noexcept {
  _terminate_requested = true;// in case request was an implicit one such as reaching to end of method
  _terminated          = true;
  update();
}

bool
InterruptableImpl::waitInitializationRequest() {
  // Use safeExecute for implicit update called by it
  return threadWaitUntil(TimePoint::max(),//
                         ALWAYS_FALSE,    //
                         [this] { return isInitializeRequested(); });
}

void
InterruptableImpl::sleepUntil(TimePoint deadline) {
  // Use safeExecute for implicit update called by it
  threadWaitUntil(deadline,    //
                  ALWAYS_FALSE,//
                  ALWAYS_FALSE);
}

void
InterruptableImpl::sleepFor(TimeUnit const &duration) {
  sleepUntil(toDeadline(duration));
}

void
InterruptableImpl::throwException(std::exception_ptr exception) {
  // Use safeExecute for implicit update called by it
  threadSafeExecute([&]() { _exception_ptr = exception; },//
                    TimePoint::max(),                     //
                    ALWAYS_FALSE,                         //
                    ALWAYS_TRUE);
}

/*************************************************************/

std::pair<InterruptableHostSPtr, InterruptableThreadSPtr>
interruptableFactory(TimeUnit response_time) {
  auto impl = std::make_shared<InterruptableImpl>(response_time);
  return std::make_pair(std::dynamic_pointer_cast<InterruptableHost>(impl),//
                        std::dynamic_pointer_cast<InterruptableThread>(impl));
}

}// namespace Ferguson::Synchronizer
