// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

/* 3rd Party */
#ifdef DT_BOOST_STACK_TRACE
#include <boost/stacktrace.hpp>
#endif

/* Local */
#include "synchronizer_impl.h"

namespace Ferguson::Synchronizer {

SynchronizerImpl::SynchronizerImpl(TimeUnit response_time): _response_time(response_time){};

bool
SynchronizerImpl::safeExecute(std::function<void()> const &procedure,//
                  TimePoint deadline,                    //
                  const NullaryBoolF &fail_if_true,      //
                  const NullaryBoolF &delay_until_true) {
  LockType lk;
  bool lock_acquired;
  std::tie(lock_acquired, lk) = acquireLock(fail_if_true);
  if(!lock_acquired) {
    return false;
  }
  while(true) {
    auto response_wait_until = Clock::now() + _response_time;

    // Condition for wait is checked before wait starts
    _update.wait_until(lk,                                                             //
                       response_wait_until < deadline ? response_wait_until : deadline,//
                       [&]() { return delay_until_true() || fail_if_true(); });
    if(delay_until_true()) {
      break;
    }
    // If signal received because of termination
    if(fail_if_true()) {
      // lk will be unlocked automatically on destruction
      return false;
    }
    if(Clock::now() >= deadline) {
      return false;
    }
  }

  timeProcedure(procedure);
  lk.unlock();
  _update.notify_all();
  return true;
}

bool
SynchronizerImpl::waitUntil(TimePoint deadline,              //
                NullaryBoolF const &fail_if_true,//
                NullaryBoolF const &delay_until_true) {
  return SynchronizerImpl::safeExecute([]() {}, deadline, fail_if_true, delay_until_true);
}

/**
 * @brief Acquire shared update lock with failure check
 * @param fail_if_true Return from the method if acquiring is no longer
 * required
 * @return a pair indicating if lock acquired successfully and lock
 */
std::pair<bool, Synchronizer::LockType>
SynchronizerImpl::acquireLock(NullaryBoolF const &fail_if_true) {
  Synchronizer::LockType lk(_update_mutex, _response_time);
  // Try until lock
  while(!lk.owns_lock()) {
    if(fail_if_true()) {
      return std::make_pair(false, std::move(lk));
    }
    lk.try_lock_for(_response_time);
  }
  return std::make_pair(true, std::move(lk));
}

/**
 * @brief signal for waiting condition to be checked
 */
void
SynchronizerImpl::update() noexcept {
  _update.notify_all();
}

void
timeProcedure(std::function<void()> const &procedure,//
              TimePoint deadline,                    //
              TimeUnit const &_response_time) {
#ifndef DEBUGGING
  auto start = Clock::now();
  procedure();
  auto end               = Clock::now();
  auto response_deadline = start + _response_time;
  deadline               = deadline < response_deadline ? deadline : response_deadline;
  auto exceeded_by       = boost::chrono::duration_cast<TimeUnit>(end - deadline).count();
  if(exceeded_by > 0) {
    auto s = boost::chrono::duration_cast<TimeUnit>(start.time_since_epoch()).count();
    auto n = boost::chrono::duration_cast<TimeUnit>(end.time_since_epoch()).count();
    auto x = boost::chrono::duration_cast<TimeUnit>(deadline.time_since_epoch()).count();
    std::stringstream ss;
    ss << "Start: " << s << std::endl;
    ss << "Now: " << n << std::endl;
    ss << "Deadline:" << x << std::endl;
    ss << "Exceeded by: " << exceeded_by << "ms" << std::endl;
#ifdef DT_BOOST_STACK_TRACE
    ss << boost::stacktrace::stacktrace() << std::endl;
#endif
    throw Ferguson::Synchronizer::Synchronizer::response_timeout_error(
        "procedure takes longer time than allowed timeout!\n" + ss.str());
  }
#else
  procedure();
#endif
}

void
SynchronizerImpl::timeProcedure(std::function<void()> const &procedure,//
                    TimePoint deadline) {
  Ferguson::Synchronizer::timeProcedure(procedure, deadline, _response_time);
}

SynchronizerSPtr
Synchronizer::factory(TimeUnit const &max_terminate_delay) {
  auto impl = std::make_shared<SynchronizerImpl>(max_terminate_delay);
  return std::dynamic_pointer_cast<Synchronizer>(impl);
}

}// namespace Ferguson::Synchronizer
