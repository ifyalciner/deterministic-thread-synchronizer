// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "ferguson/state/state.h"
#include <ferguson/impl/state/state_impl.h>

/* CPP */
#include <memory>
#include <random>


namespace Ferguson::State {

/*--------------------------------------------------------------------------------------------------------------------*/
/* StateSpaceDescription ---------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

void
StateSpaceDescription::check_dimension(std::string const &dimension) const {
  auto const &dim_it = name_to_index.find(dimension);
  if(dim_it == name_to_index.end()) {
    std::stringstream ss;
    ss << "Error: Key '" << dimension << "' doesn't match a state space dimension!" << std::endl;
    throw std::invalid_argument(ss.str());
  }
}
void
StateSpaceDescription::check_range(std::string const &dimension, uint64_t value) const {
  check_range(dimension, std::set<uint64_t>{value});
}

void
StateSpaceDescription::check_range(std::string const &dimension, std::set<uint64_t> const &value) const {
  check_dimension(dimension);
  auto dim_index = name_to_index.at(dimension);
  for(auto const v : value) {
    if(v >= dimension_size[dim_index]) {
      std::stringstream ss;
      ss << "Error: Value '" << v << "' of dimension '" << dimension << "' exceeds dimension limits [0,"
         << dimension_size[dim_index] << ")!" << std::endl;
      throw std::invalid_argument(ss.str());
    }
  }
}

StateSpaceDescription::ValType
StateSpaceDescription::get_zero_value() const {
  return ValType(space_size, 0);
}

StateSpaceDescription::ValType
StateSpaceDescription::get_bitset(ValType const &values) const {
  if(space_size != values.size()) {
    std::stringstream ss;
    ss << "Error: Value bitsize doesn't match!" << std::endl;
    throw std::invalid_argument(ss.str());
  }
  return values;
}

StateSpaceDescription::ValType
StateSpaceDescription::get_bitset(std::map<std::string, uint64_t> const &values) const {
  std::map<std::string, std::set<uint64_t>> nvalues;
  for(auto const &it : values) {
    nvalues[it.first].emplace(it.second);
  }
  return get_bitset(nvalues);
}

StateSpaceDescription::ValType
StateSpaceDescription::get_bitset(std::map<std::string, std::set<uint64_t>> const &values) const {
  StateSpaceDescription::ValType ret = get_zero_value();
  get_bitset_inner(values, ret);
  return ret;
}

void
StateSpaceDescription::get_bitset_inner(std::map<std::string, std::set<uint64_t>> const &values,// Settled values
                                        StateSpaceDescription::ValType &val,                    //
                                        uint64_t cur_index,                                     //
                                        uint64_t index) const {
  if(cur_index >= dimension_size.size()) {
    val.set(index);
    return;
  }
  for(auto const &v : values.at(index_to_name.at(cur_index))) {
    get_bitset_inner(values,       //
                     val,          //
                     cur_index + 1,//
                     index + dimension_step_size[cur_index] * v);
  }
}

std::map<std::string, uint64_t>
StateSpaceDescription::get_map(uint64_t state_bit_index) const {
  std::map<std::string, uint64_t> result;
  for(uint64_t i = 0; i < no_dimensions; ++i) {
    result[index_to_name[i]] = uint64_t(state_bit_index / dimension_step_size[i]);
    state_bit_index %= dimension_step_size[i];
  }
  return result;
}

std::map<std::string, uint64_t>
StateSpaceDescription::get_map(std::map<std::string, uint64_t> map) {
  return map;
}

std::map<std::string, uint64_t> &
StateSpaceDescription::complete_map(std::map<std::string, uint64_t> &map) const {
  for(uint64_t index = 0; index < no_dimensions; ++index) {
    map.emplace(index_to_name[index], 0);
  }
  return map;
}

std::map<std::string, std::set<uint64_t>> &
StateSpaceDescription::complete_map(std::map<std::string, std::set<uint64_t>> &map) const {
  for(uint64_t index = 0; index < no_dimensions; ++index) {
    auto const &name = index_to_name[index];
    auto ret         = map.emplace(name, std::set<uint64_t>{});
    if(ret.second) {
      for(uint64_t v = 0; v < dimension_size[index]; ++v) {
        ret.first->second.emplace(v);
      }
    }
  }
  return map;
}

uint64_t
StateSpaceDescription::sample(StateSpaceDescription::ValType const &v) {
  static std::random_device r;
  static std::default_random_engine dre(r());
  std::uniform_int_distribution<uint64_t> udist(1, v.count());
  uint64_t order = udist(dre);
  uint64_t x     = v.find_first();
  for(uint64_t i = 2; i <= order; i++) {
    x = v.find_next(x);
  }
  return x;
}

/*--------------------------------------------------------------------------------------------------------------------*/
/* AnyStateImpl ------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

template<typename T, //
         typename T2,//
         typename = typename std::enable_if<std::is_base_of<AnyStateImpl, T>::value, T>::type,
         typename = typename std::enable_if<std::is_base_of<AnyStateImpl, T2>::value, T2>::type>
void
check_space_desc(std::shared_ptr<T const> const &s, std::shared_ptr<T2 const> const &s2) {
  auto ss  = std::static_pointer_cast<AnyStateImpl const>(s);
  auto ss2 = std::static_pointer_cast<AnyStateImpl const>(s2);
  if(ss->space().get() != ss2->space().get()) {
    throw std::invalid_argument("Error: States belong to different spaces!");
  }
}

StateSpaceCSPtr
AnyStateImpl::space() const {
  return std::static_pointer_cast<StateSpace const>(_space);
}

bool
AnyStateImpl::is_subset_of(Ferguson::State::AnyStateImplCSPtr const &s) const {
  check_space_desc(this->shared_from_this(), s);
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  return val().is_subset_of(ss->val());
}

bool
AnyStateImpl::is_proper_subset_of(Ferguson::State::AnyStateImplCSPtr const &s) const {
  check_space_desc(this->shared_from_this(), s);
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  return val().is_proper_subset_of(ss->val());
}

bool
AnyStateImpl::operator==(AnyStateImplCSPtr const &s) const {
  check_space_desc(this->shared_from_this(), s);
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  return val() == ss->val() && _space == ss->_space;
}

bool
AnyStateImpl::operator!=(AnyStateImplCSPtr const &s) const {
  return !operator==(s);
}

SuperStateImplSPtr
AnyStateImpl::operator|(AnyStateImplCSPtr const &s) const {
  check_space_desc(this->shared_from_this(), s);
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  return SuperStateImpl::factory(_space,//
                                 val() | ss->val());
}

SuperStateImplSPtr
AnyStateImpl::operator&(AnyStateImplCSPtr const &s) const {
  check_space_desc(this->shared_from_this(), s);
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  return SuperStateImpl::factory(_space,//
                                 val() & ss->val());
}

SuperStateImplSPtr
AnyStateImpl::operator^(AnyStateImplCSPtr const &s) const {
  check_space_desc(this->shared_from_this(), s);
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  return SuperStateImpl::factory(_space,//
                                 val() ^ ss->val());
}

SuperStateImplSPtr
AnyStateImpl::operator~() const {
  return SuperStateImpl::factory(_space,//
                                 ~val());
}

std::ostream &
AnyStateImpl::operator<<(std::ostream &o) const {
  o << val();
  return o;
}

StateSpaceDescriptionCSPtr
AnyStateImpl::desc() const {
  return _space->desc();
}

StateImplSPtr
AnyStateImpl::sample() const {
  return StateImpl::factory(_space, desc()->sample(val()));
}

AnyStateImpl::AnyStateImpl(StateSpaceImplCSPtr space): _space(std::move(space)) {}

/*--------------------------------------------------------------------------------------------------------------------*/
/* SuperStateImpl ----------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

SuperStateImplSPtr
SuperStateImpl::operator|=(AnyStateImplCSPtr const &s) {
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  _val |= ss->val();
  return std::static_pointer_cast<SuperStateImpl>(this->shared_from_this());
}

SuperStateImplSPtr
SuperStateImpl::operator&=(AnyStateImplCSPtr const &s) {
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  _val &= ss->val();
  return std::static_pointer_cast<SuperStateImpl>(this->shared_from_this());
}

SuperStateImplSPtr
SuperStateImpl::operator^=(AnyStateImplCSPtr const &s) {
  auto ss = std::static_pointer_cast<AnyStateImpl const>(s);
  _val ^= ss->val();
  return std::static_pointer_cast<SuperStateImpl>(this->shared_from_this());
}

void
SuperStateImpl::set(std::map<std::string, std::set<uint64_t>> values) {
  desc()->complete_map(values);
  _val = desc()->get_bitset(values);
}

StateSpaceDescription::ValType
SuperStateImpl::val() const {
  return _val;
}

std::vector<StateImplSPtr>
SuperStateImpl::get_states() const {
  std::vector<StateImplSPtr> result;
  uint64_t val_index = this->val().find_first();
  while(val_index != StateSpaceDescription::ValType::npos) {
    result.push_back(StateImpl::factory(_space, val_index));
    val_index = this->val().find_next(val_index);
  }
  return result;
}

AnyStateImplSPtr
SuperStateImpl::copy() {
  return SuperStateImpl::factory(*this);
}

SuperStateImplSPtr
SuperStateImpl::factory(SuperStateImpl const &s) {
  return std::shared_ptr<SuperStateImpl>(new SuperStateImpl(s));
}

SuperStateImplSPtr
SuperStateImpl::factory(SuperStateImpl &&s) {
  return std::shared_ptr<SuperStateImpl>(new SuperStateImpl(std::move(s)));
}

SuperStateImplSPtr
SuperStateImpl::operator=(SuperStateImpl const &s) {
  if(space() != s.space()) {
    throw std::invalid_argument("States doesn't belong to same state space.");
  }
  _val = s._val;
  return std::static_pointer_cast<SuperStateImpl>(this->shared_from_this());
}

SuperStateImplSPtr
SuperStateImpl::operator=(SuperStateImpl &&s)  noexcept {
  if(space() != s.space()) {
    throw std::invalid_argument("States doesn't belong to same state space.");
  }
  _val = std::move(s._val);
  return std::static_pointer_cast<SuperStateImpl>(this->shared_from_this());
}

SuperStateImpl::SuperStateImpl(SuperStateImpl const &s)
    : AnyStateImpl(static_cast<AnyStateImpl const &>(s)),//
      _val(s._val) {}

SuperStateImpl::SuperStateImpl(SuperStateImpl &&s)
 noexcept     : AnyStateImpl(static_cast<AnyStateImpl &&>(s)),//
      _val(std::move(s._val)) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------*/
/* StateImpl ---------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

void
StateImpl::set(std::string const &dimension, uint64_t value) {
  desc()->check_range(dimension, value);
  _val[dimension] = value;
}

uint64_t
StateImpl::at(std::string const &dimension) const {
  return _val.at(dimension);
}

State::ValProxy
StateImpl::at(std::string const &dimension) {
  return {std::static_pointer_cast<StateImpl>(this->shared_from_this()), dimension};
}

StateSpaceDescription::ValType
StateImpl::val() const {
  return desc()->get_bitset(_val);
}

AnyStateImplSPtr
StateImpl::copy() {
  return StateImpl::factory(*this);
}

std::shared_ptr<StateImpl>
StateImpl::factory(StateImpl const &s) {
  return std::shared_ptr<StateImpl>(new StateImpl(s));
}

std::shared_ptr<StateImpl>
StateImpl::factory(StateImpl &&s) {
  return std::shared_ptr<StateImpl>(new StateImpl(std::move(s)));
}

StateImplSPtr
StateImpl::operator=(StateImpl const &s) {
  _val = s._val;
  return std::static_pointer_cast<StateImpl>(this->shared_from_this());
}

StateImplSPtr
StateImpl::operator=(StateImpl &&s)  noexcept {
  _val = std::move(s._val);
  return std::static_pointer_cast<StateImpl>(this->shared_from_this());
}

StateImpl::StateImpl(StateImpl const &s)
    : AnyStateImpl(static_cast<AnyStateImpl const &>(s)),//
      _val(s._val) {}

StateImpl::StateImpl(StateImpl &&s)
 noexcept     : AnyStateImpl(static_cast<AnyStateImpl &&>(s)),//
      _val(std::move(s._val)) {}

/*--------------------------------------------------------------------------------------------------------------------*/
/* StateSpaceImpl ----------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

/**
 * @brief
 * @param state_dimensions: A map of super_state variable name and range size
 */
StateSpaceImpl::StateSpaceImpl(std::map<std::string, uint64_t> const &dimensions) {
  std::vector<std::string> names;
  std::vector<uint64_t> dimension_size;
  std::map<std::string, uint64_t> name_to_index;
  std::vector<uint64_t> dimension_step_size;

  uint64_t index = 0;
  for(auto const &it : dimensions) {
    names.push_back(it.first);
    dimension_size.push_back(it.second);
    name_to_index[it.first] = index++;
    dimension_step_size.emplace_back(1);
  }

  for(int64_t i = int64_t(dimension_step_size.size()) - 2; i >= 0; --i) {
    dimension_step_size[i] = dimension_step_size[i + 1] * dimension_size[i + 1];
  }
  _space_desc = StateSpaceDescriptionCSPtr(
      new StateSpaceDescription{index,                                                                                //
                                names,                                                                                //
                                std::unordered_map<std::string, uint64_t>(name_to_index.begin(), name_to_index.end()),//
                                dimension_size,                                                                       //
                                dimension_step_size,                                                                  //
                                dimension_step_size[0] * dimension_size[0]});
}

State
StateSpaceImpl::state(std::map<std::string, uint64_t> values) const {
  _space_desc->check_health(values);
  _space_desc->complete_map(values);
  return State{StateImpl::factory(this->shared_from_this(),//
                                  values)};
}

SuperState
StateSpaceImpl::super_state(std::map<std::string, std::set<uint64_t>> values) const {
  _space_desc->check_health(values);
  _space_desc->complete_map(values);
  return SuperState{SuperStateImpl::factory(this->shared_from_this(),//
                                            values)};
}

SuperState
StateSpaceImpl::empty_super_state() const {
  return SuperState{SuperStateImpl::factory(this->shared_from_this(),//
                                            _space_desc->get_zero_value().reset())};
}

SuperState
StateSpaceImpl::power_super_state() const {
  return SuperState{SuperStateImpl::factory(this->shared_from_this(),//
                                            _space_desc->get_zero_value().set())};
}

StateSpaceDescriptionCSPtr
StateSpaceImpl::desc() const {
  return _space_desc;
}

/*--------------------------------------------------------------------------------------------------------------------*/

//std::ostream &
//SuperStateImpl::operator<<(std::ostream &o) const {
//  o << "{" << std::endl;
//  for(auto const &i : _state_values) {
//    o << "  " << i.first << " : " << i.second << std::endl;
//  }
//  o << "}" << std::endl;
//  return o;
//}
//std::ostream &
//SuperStateImplRegion::operator<<(std::ostream &o) {
//  o << "{" << std::endl;
//  for(auto const &i : _state_region) {
//    o << "  " << i.first << " : " << i.second.first << "-" << i.second.second << std::endl;
//  }
//  o << "}" << std::endl;
//  return o;
//}
/*--------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

} // namespace Ferguson::State

