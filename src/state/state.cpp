// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "ferguson/state/state.h"
#include <ferguson/impl/state/state_impl.h>

namespace Ferguson::State {

inline void
check_space(AnyState const &x, AnyState const &y) {
  if(x.pimpl()->space() != y.pimpl()->space()) {
    throw std::invalid_argument("State spaces don't match!");
  }
}

/*--------------------------------------------------------------------------------------------------------------------*/
/* AnySpace ----------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

StateSpaceCSPtr
AnyState::space() const {
  return _pimpl->space();
}
bool
AnyState::is_subset_of(AnyState const &s) const {
  return _pimpl->is_subset_of(s._pimpl);
}

bool
AnyState::is_proper_subset_of(AnyState const &s) const {
  return _pimpl->is_proper_subset_of(s._pimpl);
}

bool
AnyState::operator==(AnyState const &s) const {
  return _pimpl->operator==(s._pimpl);
}

bool
AnyState::operator!=(AnyState const &s) const {
  return _pimpl->operator!=(s._pimpl);
}

SuperState
AnyState::operator|(AnyState const &s) const {
  return SuperState{std::static_pointer_cast<SuperStateImpl>(_pimpl->operator|(s._pimpl))};
}

SuperState
AnyState::operator&(AnyState const &s) const {
  return SuperState{std::static_pointer_cast<SuperStateImpl>(_pimpl->operator&(s._pimpl))};
}

SuperState
AnyState::operator^(AnyState const &s) const {
  return SuperState{std::static_pointer_cast<SuperStateImpl>(_pimpl->operator^(s._pimpl))};
}

SuperState
AnyState::operator~() const {
  return SuperState{std::static_pointer_cast<SuperStateImpl>(_pimpl->operator~())};
}

std::ostream &
AnyState::operator<<(std::ostream &o) const {
  return _pimpl->operator<<(o);
}

AnyStateImplCSPtr
AnyState::pimpl() const {
  return _pimpl;
}

State
AnyState::sample() const {
  return State{_pimpl->sample()};
}

/**
 * @details call copy factory method for AnyStateImpl
 */
AnyState::AnyState(AnyState const &s): _pimpl(s._pimpl->copy()) {}
/**
 * @details Moving object in interface level is easier
 */
AnyState::AnyState(AnyState &&s) noexcept {
  _pimpl   = std::move(s._pimpl);
  s._pimpl = nullptr;
}

AnyState &
AnyState::operator=(AnyState const &s) {
  if(this == &s) {
    return *this;
  }
  check_space(*this, s);
  _pimpl = s._pimpl->copy();
  return *this;
}

/**
 * @details Moving object in interface level is easier
 */
AnyState &
AnyState::operator=(AnyState &&s) noexcept {
  if(this == &s) {
    return *this;
  }
  check_space(*this, s);
  _pimpl   = std::move(s._pimpl);// simply move the pimpl
  s._pimpl = nullptr;
  return *this;
}

//AnyState::AnyState(AnyState const &s) {
//  _pimpl = AnyStateImpl::factory(std::static_pointer_cast<AnyStateImpl>(s._pimpl));
//}
//
//AnyState::AnyState(AnyState &&s) {
//  _pimpl   = s._pimpl;
//  s._pimpl = nullptr;
//}
//
//AnyState &
//AnyState::AnyState::operator=(AnyState const &s) {
//  _pimpl = AnyStateImpl::factory(*s._pimpl);
//  return *this;
//}
//
//AnyState &
//AnyState::AnyState::operator=(AnyState &&s) {
//  _pimpl   = s._pimpl;
//  s._pimpl = nullptr;
//  return *this;
//}

AnyState::AnyState(AnyStateImplSPtr pimpl): _pimpl(std::move(pimpl)) {}

/*--------------------------------------------------------------------------------------------------------------------*/
/* SuperState --------------------------------------------------------------------------------------------------------*/

SuperState
SuperState::operator|=(AnyState const &s) {
  std::static_pointer_cast<SuperStateImpl>(_pimpl)->operator|=(s.pimpl());
  return *this;
}

SuperState
SuperState::operator&=(AnyState const &s) {
  std::static_pointer_cast<SuperStateImpl>(_pimpl)->operator&=(s.pimpl());
  return *this;
}

SuperState
SuperState::operator^=(AnyState const &s) {
  std::static_pointer_cast<SuperStateImpl>(_pimpl)->operator^=(s.pimpl());
  return *this;
}

void
SuperState::set(const std::map<std::string, std::set<uint64_t>> &values) {
  return std::static_pointer_cast<SuperStateImpl>(_pimpl)->set(values);
}

std::vector<State>
SuperState::get_states() const {
  auto v = std::static_pointer_cast<SuperStateImpl>(_pimpl)->get_states();
  std::vector<State> ret;
  std::transform(v.begin(), v.end(), std::back_inserter(ret),
                 [](StateImplSPtr const &s) { return State(std::static_pointer_cast<AnyStateImpl>(s)); });
  return ret;
}

/**
 * @details call copy factory method for StateImpl
 */
SuperState::SuperState(SuperState const &s)//
    : AnyState(s) {}

/**
 * @details Moving object in interface level is easier
 */
SuperState::SuperState(SuperState &&s) noexcept//
    : AnyState(std::move(s)) {}

SuperState &
SuperState::operator=(SuperState const &s) {
  *std::static_pointer_cast<SuperStateImpl>(_pimpl) = *std::static_pointer_cast<SuperStateImpl>(s._pimpl);
  return *this;
}

/**
 * @details Moving object in interface level is easier
 */
SuperState &
SuperState::operator=(SuperState &&s) noexcept {
  AnyState::operator=(std::move(s));
  return *this;
}

SuperState::SuperState(AnyStateImplSPtr pimpl): AnyState(std::move(pimpl)) {}

/*--------------------------------------------------------------------------------------------------------------------*/
/* State --------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

State::ValProxy::operator uint64_t() const { return std::static_pointer_cast<StateImpl const>(_state)->at(_dimension); }

State::ValProxy &
State::ValProxy::operator=(uint64_t val) {
  _state->set(_dimension, val);
  return *this;
}

State::ValProxy::ValProxy(StateImplSPtr s,//
                          std::string dimension)
    : _state(std::move(s)),//
      _dimension(std::move(dimension)) {}

//--------------------------------------------------------------------------------------------------

void
State::set(std::string const &dimension, uint64_t value) {
  return std::static_pointer_cast<StateImpl>(_pimpl)->set(dimension, value);
}

State::ValProxy
State::at(std::string const &dimension) {
  return {std::static_pointer_cast<StateImpl>(_pimpl), dimension};
}

uint64_t
State::at(std::string const &dimension) const {
  return std::static_pointer_cast<StateImpl const>(_pimpl)->at(dimension);
}

State::ValProxy
State::operator[](std::string const &dimension) {
  return {std::static_pointer_cast<StateImpl>(_pimpl), dimension};
}

uint64_t
State::operator[](std::string const &dimension) const {
  return std::static_pointer_cast<StateImpl const>(_pimpl)->at(dimension);
}

/**
 * @details call copy factory method for StateImpl
 */
State::State(State const &s)//
    : AnyState(s) {}

/**
 * @details Moving object in interface level is easier
 */
State::State(State &&s) noexcept//
    : AnyState(std::static_pointer_cast<AnyStateImpl>(s._pimpl)) {
  s._pimpl = nullptr;
}

State &
State::operator=(State const &s) {
  _pimpl = s._pimpl->copy();
  return *this;
}

/**
 * @details Moving object in interface level is easier
 */
State &
State::operator=(State &&s) noexcept {
  _pimpl   = std::move(s._pimpl);// simply move the pimpl
  s._pimpl = nullptr;
  return *this;
}

State::State(AnyStateImplSPtr pimpl): AnyState(std::move(pimpl)) {}

/*--------------------------------------------------------------------------------------------------------------------*/
/* StateSpace --------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------*/

StateSpaceCSPtr
StateSpace::factory(std::map<std::string, uint64_t> const &dimensions) {
  return std::shared_ptr<StateSpace const>(new StateSpaceImpl(dimensions));
}

}// namespace Ferguson::State
