// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <atomic>
#include <cstdlib>
#include <ferguson/time.h>

namespace Ferguson {

#ifdef DEBUGGING
static std::atomic_bool terminated(false);
TimePoint
Clock::now() noexcept {
  static std::atomic<int> now_tp(0);
  static boost::thread incrementor_task = boost::thread([&]() {
    while(!terminated) {
      now_tp += 1;
      boost::this_thread::sleep_for(TimeUnit(1));
    }
  });
  std::atexit([] {
    terminated = true;
    if(incrementor_task.joinable()) {
      incrementor_task.join();
    }
  });
  return TimePoint(TimeUnit(now_tp));
}
#endif

TimePoint
toDeadline(TimeUnit const &timeout, TimePoint start) {
  start = start == TimePoint() ? Clock::now() : start;
  if(timeout == TimeUnit::max() || TimePoint::max() - start < timeout) {
    return TimePoint::max();
  }
  else {
    return start + timeout;
  }
}
}// namespace Ferguson
